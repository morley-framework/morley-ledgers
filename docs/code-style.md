<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Haskell Style Guide

Haskell code in this repository adheres to the same style as in [morley](https://gitlab.com/morley-framework/morley/-/blob/master/docs/code-style.md).
