# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

.PHONY: all stylish clean

# Build target from the common utility Makefile
MAKEU = $(MAKE) -f code/make/Makefile

ifeq (${MORLEY_USE_CABAL},1)
	build_all = cabal v2-build --enable-tests --enable-benchmarks -O0 all
	test_all = cabal v2-test -O0 all
	clean_all = cabal v2-clean
else
	test_all = $(MAKEU) test PACKAGE=""
	build_all = $(MAKEU) PACKAGE=""
	clean_all = stack clean
endif

all:
	$(call build_all,)
stylish:
	find code/ -name '.stack-work' -prune -o -name '*.hs' -exec stylish-haskell -i {} \;
clean:
	$(call clean_all,)
