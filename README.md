<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

> :warning: **Note: this project is deprecated.**
>
> It is no longer maintained since the activation of protocol "Nairobi" on the Tezos mainnet (June 24th, 2023).

# Morley Ledgers

[![pipeline status](https://gitlab.com/morley-framework/morley-ledgers/badges/master/pipeline.svg)](https://gitlab.com/morley-framework/morley-ledgers/-/commits/master)

This repository contains two Haskell packages:
* `morley-ledgers` contains ledger smart contracts developed using the Morley framework.
They implement TZIP standards such as FA1.2 ([TZIP-7](https://gitlab.com/tzip/tzip/-/blob/eb28207e250119a4753ddef1f21390e75cdac0a0/proposals/tzip-7/tzip-7.md)).
* `morley-ledgers-test` package contains test-suites for ledger smart contracts.
They are generalized to various extent and can work with many smart contracts satisfying certain criteria (for example, some tests should hold for any FA1.2-compliant smart contract).

## Build and usage instructions

1. You can use these packages as libraries.
They are not on Hackage yet, so you have to add them by reference to this repository to your `stack.yaml` or `cabal.project` file.
2. There is `morley-ledgers` executable that you can use to print smart contracts in Michelson or Micheline format, print initial storages, documentation, etc.
In order to build and use it, you need [Stack](https://docs.haskellstack.org/en/stable/README/) or [Cabal](https://www.haskell.org/cabal/).

## For Contributors

Please see [CONTRIBUTING.md](CONTRIBUTING.md).
