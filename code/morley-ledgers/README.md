# Morley Legders

This package contains contracts implemented using Haskell eDSL for Michelson based on Morley.
The contracts implement common Ledger interfaces:
* `AbstractLedger` contract provides a trivial implementation of the AbstractLedger interface.
* `ManagedLedger` contract provides a sample implementation of the ApprovableLedger interface.
* `FA2` contract provides a sample implementation of the FA2 interface.
It's implemented in Lorentz and Indigo (two different implementations).

## Contract registry

To read contracts defined in this package one can use the `morley-ledgers` executable.

Example (assuming the executable is installed into `$PATH`, if it's not – add `stack exec` or `cabal run`):
```sh
make
stack exec morley-ledgers -- list
```
Shows names of the contracts.

```sh
stack exec morley-ledgers -- print -n ManagedLedger
```
Prints the code of the `ManagedLedger` contract.

## Contracts documentation

Contracts have their documentation published and automatically updated by CI:
* [AbstractLedger](https://gitlab.com/morley-framework/morley-ledgers/blob/autodoc/master/autodoc/AbstractLedger.md)
* [ManagedLedger](https://gitlab.com/morley-framework/morley-ledgers/blob/autodoc/master/autodoc/ManagedLedger.md)
* [FA2Sample](https://gitlab.com/morley-framework/morley-ledgers/blob/autodoc/master/autodoc/FA2Sample.md)
