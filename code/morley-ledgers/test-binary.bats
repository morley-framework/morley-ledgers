#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

setup () {
  bin_dir="tmp"
}

@test "Morley ledgers registry can print ManagedLedger and AbstractLedger" {
  $bin_dir/morley-ledgers -- print -n ManagedLedger
  $bin_dir/morley-ledgers -- print -n AbstractLedger
  $bin_dir/morley-ledgers -- print -n AbstractLedger --micheline
  $bin_dir/morley-ledgers -- storage-AbstractLedger --micheline
}

@test "Morley ledgers registry can analyze its contracts" {
  $bin_dir/morley-ledgers -- analyze -n AbstractLedger
  $bin_dir/morley-ledgers -- analyze -n ManagedLedger
}
