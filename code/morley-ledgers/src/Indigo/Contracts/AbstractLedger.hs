-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Abstract ledger implementation as described in TZIP-FA1.

module Indigo.Contracts.AbstractLedger
  ( Parameter (..)
  , Storage (..)
  , emptyStorage
  , abstractLedgerContract
  ) where

import Indigo
import Lorentz.Run (Contract)

import Lorentz.Contracts.Spec.AbstractLedgerInterface
import Test.Cleveland.Instances ()

data Storage = Storage
  { sLedger      :: BigMap Address Natural
  , sTotalSupply :: Natural
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)

instance TypeHasDoc Storage where
  typeDocMdDescription =
    "Contract's storage holding a big_map with all balances and the total supply."

emptyStorage :: Storage
emptyStorage = Storage { sLedger = mempty, sTotalSupply = 0 nat}

----------------------------------------------------------------------------
-- Entrypoints
----------------------------------------------------------------------------

transfer
  :: HasStorage Storage
  => Var TransferParams
  -> IndigoProcedure
transfer tp = do
  doc $ DDescription "Transfers tokens between two given accounts."

  to <- new$ tp #! #to
  from <- new$ tp #! #from
  value <- new$ tp #! #value

  debitFrom from value
  creditTo to value

debitFrom
  :: forall from value.
     ( from :~> Address, value :~> Natural
     , HasStorage Storage
     )
  => from -> value -> IndigoProcedure
debitFrom from val = do
  -- Sender check.
  when (from /= Sender) do
    failCustom @() #nonAcceptableSource $ pair
      (Sender !~ #sender)
      (from !~ #from)

  -- Balance check and update.
  ledger_ <- getStorageField  @Storage #sLedger
  ifSome (ledger_ #: from)
    (\oldBalance -> do
      newBalance <- ifSome (isNat $ oldBalance - val)
        return (failNotEnoughBalance @Natural oldBalance)

      setStorageField @Storage #sLedger $ ledger_ !: (from, nonZero newBalance)
    )
    (failNotEnoughBalance @() (0 nat))
  where
    failNotEnoughBalance :: forall r ex. (ex :~> Natural, ReturnableValue r) => ex -> IndigoM (RetVars r)
    failNotEnoughBalance curBalance = failCustom @r #notEnoughBalance $ pair
      (val !~ #required)
      (curBalance !~ #present)

creditTo
  :: forall to value.
     ( to :~> Address, value :~> Natural
     , HasStorage Storage
     )
  => to -> value -> IndigoProcedure
creditTo to value = do
  updateStorageField @Storage #sLedger $ \ledger -> do
    newBalance <- ifSome (ledger #: to)
      (\oldBalance -> return $ oldBalance + value)
      (new value)

    return $ ledger +: (to, newBalance)

getTotalSupply
  :: (HasSideEffects, HasStorage Storage, IsNotInView)
  => Var GetTotalSupplyArg
  -> IndigoProcedure
getTotalSupply v = do
  doc $ DDescription "Returns total number of tokens."
  project v $ \_ -> getStorageField @Storage #sTotalSupply

getBalance
  :: (HasSideEffects , HasStorage Storage, IsNotInView)
  => Var GetBalanceArg
  -> IndigoProcedure
getBalance v = do
  doc $ DDescription "Returns the balance of the address in the ledger."
  project v $ \owner -> do
    ledger_ <- getStorageField @Storage #sLedger
    ifSome (ledger_ #: unName #owner owner) return (return $ 0 nat)

-------------------------------------------------------------------------------
-- Contract
----------------------------------------------------------------------------

abstractLedgerContract :: Contract Parameter Storage ()
abstractLedgerContract = mkContract $ compileIndigoContract abstractLedgerIndigo

abstractLedgerIndigo
  :: (HasStorage Storage, HasSideEffects, IsNotInView)
  => Var Parameter
  -> IndigoProcedure
abstractLedgerIndigo param = docGroup "Abstract Ledger" do
  contractGeneralDefault
  doc(dStorage @Storage)
  doc $ DDescription contractDoc
  entryCaseSimple param
    ( #cTransfer #= transfer
    , #cGetTotalSupply #= getTotalSupply
    , #cGetBalance #= getBalance
    )
  where
    contractDoc :: Markdown
    contractDoc =
      "This documentation describes a minimal abstract ledger contract \
      \that is described in TZIP-FA1."
