-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Managed ledger contract Indigo variant.

module Indigo.Contracts.ManagedLedger
  ( -- * Parameter
    Parameter (..)

  -- * Storage
  , Storage
  , StorageC
  , mkStorage
  , LedgerValue

  -- * Contract
  , managedLedgerContract

  -- * Parts of the implementation that can be reused
  , approve
  , burn
  , creditTo
  , debitFrom
  , ensureNotPaused
  , getAllowance
  , getBalance
  , getTotalSupply
  , mint
  , setPause
  , transfer
  ) where

import Indigo
import Lorentz (Contract)

import Lorentz.Contracts.ManagedLedger.Doc qualified as L
import Lorentz.Contracts.ManagedLedger.Types
import Lorentz.Contracts.Spec.ApprovableLedgerInterface qualified as AL
import Lorentz.Contracts.Spec.ManagedLedgerInterface

type IStorageC s =
  ( ILedgerC s
  , HasField s "admin" Address
  , HasField s "paused" Bool
  )

type ILedgerC s =
  ( HasField s "ledger" (BigMap Address LedgerValue)
  , HasField s "approvals" (BigMap GetAllowanceParams Natural)
  , HasField s "totalSupply" Natural
  , HasStorage s
  , IsObject s
  )

----------------------------------------------------------------------------
-- Entrypoints
----------------------------------------------------------------------------

-- | Transfers tokens between two given accounts.
transfer
  :: forall s tp.
     ( tp :~> AL.TransferParams
     , ILedgerC s, HasField s "paused" Bool
     )
  => IndigoEntrypoint tp
transfer parameter = do
  doc $ DDescription L.transferDoc

  ensureNotPaused @s

  to <- new$ parameter #! #to
  from <- new$ parameter #! #from
  value <- new$ parameter #! #value

  when (sender /= from) do
    consumeAllowance @s parameter

  debitFrom @s from value
  creditTo @s to value

-- | Allows spender to withdraw tokens from sender.
approve
  :: forall s ap.
     ( ap :~> ApproveParams
     , ILedgerC s, HasField s "paused" Bool
     )
  => IndigoEntrypoint ap
approve parameter = do
  doc $ DDescription L.approveDoc

  ensureNotPaused @s

  spender <- new$ parameter #! #spender
  value <- new$ parameter #! #value

  currentAllowance <- allowance @s $ construct
    ( sender !~ #owner
    , spender !~ #spender
    )

  when (currentAllowance /= 0 nat && value /= 0 nat) $
    failCustom @() #unsafeAllowanceChange currentAllowance

  setAllowance @s sender spender value

-- | Compares the expected allowance value with the actual one
-- and sets a new one if they match.
approveCAS
  :: forall s ap.
     ( ap :~> ApproveCasParams
     , ILedgerC s, HasField s "paused" Bool
     )
  => IndigoEntrypoint ap
approveCAS parameter = do
  doc $ DDescription L.approveCASDoc

  ensureNotPaused @s

  spender <- new$ parameter #! #spender
  value <- new$ parameter #! #value
  expectedAllowance <- new$ parameter #! #expected

  actualAllowance <- allowance @s $ construct
    ( sender !~ #owner
    , spender !~ #spender
    )

  allowanceMismatchError <- new$ pair
    (actualAllowance !~ #actual)
    (expectedAllowance !~ #expected)

  when (expectedAllowance /= actualAllowance) $
    failCustom @() #allowanceMismatch allowanceMismatchError

  setAllowance @s sender spender value

-- | Returns the approval value between two given addresses.
getAllowance
  :: forall s ap.
     ( ap :~> AL.GetAllowanceArg
     , ILedgerC s
     , HasSideEffects
     , IsNotInView
     )
  => IndigoEntrypoint ap
getAllowance parameter = do
  doc $ DDescription L.getAllowanceDoc
  project parameter (allowance @s)

-- | Returns current balance of the addres in ledger.
getBalance
  :: forall s ap.
     ( ap :~> AL.GetBalanceArg
     , ILedgerC s
     , HasSideEffects
     , IsNotInView
     )
  => IndigoEntrypoint ap
getBalance parameter = do
  doc $ DDescription L.getBalanceDoc

  ledger_ <- getStorageField @s #ledger

  project parameter $ \owner -> do
    ifSome (ledger_ #: unName #owner owner)
      (\balance' -> return $ balance' #~ #balance)
      (return $ 0 nat)

-- | Returns total number of tokens.
getTotalSupply
  :: forall s tp.
     ( tp :~> AL.GetTotalSupplyArg
     , ILedgerC s
     , HasSideEffects
     , IsNotInView
     )
  => IndigoEntrypoint tp
getTotalSupply parameter = do
  doc $ DDescription L.getTotalSupplyDoc
  project parameter $ \_ -> getStorageField @s #totalSupply

-- | Pauses transfer and approval operations if this parameter is set to `True`.
setPause
  :: forall s tp.
     ( tp :~> Bool
     , IStorageC s
     )
  => IndigoEntrypoint tp
setPause parameter = do
  doc $ DDescription L.setPauseDoc
  authorizeAdmin @s
  setStorageField @s #paused parameter

-- | Changes current administrator.
setAdministrator
  :: forall s sp.
     ( sp :~> Address
     , IStorageC s
     )
  => IndigoEntrypoint sp
setAdministrator parameter = do
  doc $ DDescription L.setAdministratorDoc
  authorizeAdmin @s
  setStorageField @s #admin parameter

-- | Returns current administrator address.
getAdministrator
  :: forall s gp.
     ( gp :~> View_ () Address
     , IStorageC s
     , HasSideEffects
     , IsNotInView
     )
  => IndigoEntrypoint gp
getAdministrator parameter = do
  doc $ DDescription L.getAdministratorDoc
  project parameter $ \_ -> getStorageField @s #admin

-- | Produces token for the given address.
mint
  :: forall s mp.
     ( mp :~> MintParams
     , IStorageC s
     )
  => IndigoEntrypoint mp
mint parameter = do
  doc $ DDescription L.mintDoc
  authorizeAdmin @s
  creditTo @s (parameter #! #to) (parameter #! #value)

-- | Reduces the given amount of tokens for the given address.
burn
  :: forall s bp.
     ( bp :~> BurnParams
     , IStorageC s
     )
  => IndigoEntrypoint bp
burn parameter = do
  doc $ DDescription L.burnDoc
  authorizeAdmin @s
  debitFrom @s (parameter #! #from) (parameter #! #value)

----------------------------------------------------------------------------
--  Helpers
----------------------------------------------------------------------------

-- | Debit the given amount of tokens from a given address in ledger.
debitFrom
  :: forall s from value.
     ( from :~> Address, value :~> Natural
     , ILedgerC s
     )
  => from -> value -> IndigoProcedure
debitFrom from val = do
  -- Balance check and the corresponding update.

  ledger_ <- getStorageField @s #ledger

  oldBalance <- ifSome (ledger_ #: from)
    (\ledgerValue -> return $ ledgerValue #~ #balance)
    (return $ 0 nat)
  newBalance <- ifSome (isNat $ oldBalance - val) return $
    failCustom @Natural #notEnoughBalance $
      pair (val !~ #required) (oldBalance !~ #present)

  newLedgerValue <- nonEmptyLedgerValue (newBalance !~ #balance)
  setStorageField @s #ledger $ ledger_ !: (from, newLedgerValue)

  decTotalSupply @s val


-- | Credit the given amount of tokens to a given address in ledger.
creditTo
  :: forall s to value.
     ( to :~> Address, value :~> Natural
     , ILedgerC s
     )
  => to -> value -> IndigoProcedure
creditTo to value = do
  ledger_ <- getStorageField @s #ledger

  when (value /= 0 nat) do
    newBalance <- ifSome (ledger_ #: to)
      (\ledgerValue -> return $ (value + ledgerValue #~ #balance) !~ #balance)
      (return $ value !~ #balance)

    setStorageField @s #ledger $ ledger_ +: (to, newBalance)
    incTotalSupply @s value

-- | Return current allowance for spender.
allowance
  :: forall s ap.
     ( ap :~> GetAllowanceParams
     , ILedgerC s
     )
  => ap -> IndigoFunction Natural
allowance parameter = do
  approvals <- getStorageField @s #approvals
  ifSome (approvals #: parameter) return (return $ 0 nat)

-- | Set allowance for spender.
setAllowance
  :: forall s owner spender value.
     ( spender :~> Address, owner :~> Address , value :~> Natural
     , ILedgerC s
     )
  => owner -> spender -> value -> IndigoProcedure
setAllowance owner spender value = do
  approvals <- getStorageField @s #approvals
  gap <- new$ construct
    ( owner   !~ #owner
    , spender !~ #spender
    )

  setStorageField @s #approvals $ approvals !: (gap, nonZero value)

-- | Consume the given allowance from address.
consumeAllowance
  :: forall s ap.
     ( ap :~> TransferParams
     , ILedgerC s
     )
  => IndigoEntrypoint ap
consumeAllowance parameter = do
  from <- new$ parameter #! #from
  value <- new$ parameter #! #value

  currentAllowance <- allowance @s $ construct
    ( from !~ #owner
    , sender !~ #spender
    )

  notEnoughAllowanceArg <- new$ pair
    (value !~ #required)
    (currentAllowance !~ #present)

  ifSome (isNat $ currentAllowance - value)
    (\updatedAmount -> setAllowance @s from sender updatedAmount)
    (failCustom @() #notEnoughAllowance notEnoughAllowanceArg)

-- | Increase total supply by given value.
incTotalSupply
  :: forall s amount.
     ( amount :~> Natural
     , ILedgerC s
     )
  => IndigoEntrypoint amount
incTotalSupply incAmount = updateStorageField @s #totalSupply $ \totalSupply ->
  return $ totalSupply + incAmount

-- | Decrease total supply by given value, fail if total supply
-- is negative.
decTotalSupply
  :: forall s amount.
     ( amount :~> Natural
     , ILedgerC s
     )
  => IndigoEntrypoint amount
decTotalSupply decAmount = updateStorageField @s #totalSupply $ \totalSupply ->
  ifSome (isNat $ totalSupply - decAmount)
    return (failUnexpected_ @Natural [mt|Negative total supply|])

-- | Ensure that given 'LedgerValue' value cannot be safely removed
-- and return it.
nonEmptyLedgerValue
  :: ( ledgerValue :~> LedgerValue )
  => ledgerValue -> IndigoFunction (Maybe LedgerValue)
nonEmptyLedgerValue ledgerValue =
  if ledgerValue #~ #balance == 0 nat
    then return none
    else return (some ledgerValue)

-- | Ensure that operations are not paused.
ensureNotPaused
  :: forall s. (HasField s "paused" Bool, HasStorage s)
  => IndigoProcedure
ensureNotPaused = do
  doc $ L.DTokenNotPausedOnly
  paused <- getStorageField @s #paused

  when paused $
    failCustom_ @() #tokenOperationsArePaused

-- | Ensure that sender is admin.
authorizeAdmin
  :: forall s. (HasStorage s, HasField s "admin" Address)
  => IndigoProcedure
authorizeAdmin = do
  doc $ L.DRequireRole "administrator"
  currentAdmin <- getStorageField @s #admin
  when (sender /= currentAdmin) $
    failCustom_ @() #senderIsNotAdmin

----------------------------------------------------------------------------
-- Contract
----------------------------------------------------------------------------

managedLedgerContract :: Contract Parameter Storage ()
managedLedgerContract = mkContract $ compileIndigoContract managedLedgerIndigo

managedLedgerIndigo :: IndigoContract Parameter Storage
managedLedgerIndigo param = docGroup "Managed Ledger" do
  contractGeneralDefault
  doc (dStorage @Storage)
  doc $ DDescription L.contractDoc
  entryCaseSimple param
    ( #cTransfer #= transfer @Storage
    , #cApprove #= approve @Storage
    , #cApproveCAS #= approveCAS @Storage
    , #cGetAllowance #= getAllowance @Storage
    , #cGetBalance #= getBalance @Storage
    , #cGetTotalSupply #= getTotalSupply @Storage
    , #cSetPause #= setPause @Storage
    , #cSetAdministrator #= setAdministrator @Storage
    , #cGetAdministrator #= getAdministrator @Storage
    , #cMint #= mint @Storage
    , #cBurn #= burn @Storage
    )
