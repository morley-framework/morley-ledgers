-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA
{-# OPTIONS_GHC -Wno-orphans #-}

-- | FA2 Sample contract

module Indigo.Contracts.FA2Sample
  ( FA2SampleParameter
  , Ledger
  , LedgerInner
  , Operators
  , Storage (..)
  , FA2Config (..)
  , defaultPermissionDescriptor
  , defPermissionDescriptor
  , defTestPermissionDescriptor
  , permissionsDescriptor
  , fa2Contract
  , mkStorage

  -- * fa2 storage, entrypoints and support functions
  , IsStorageC
  , transfer
  , balanceOf
  , debitFrom
  , creditTo
  , updateOperators
  , addOperator
  , removeOperator
  ) where

import Indigo
import Lorentz.Contracts.Spec.FA2Interface
import Lorentz.Contracts.Spec.FA2Interface.ParameterInstances ()
import Lorentz.Run (Contract)
import Morley.Util.Markdown
import Test.Cleveland.Instances ()


contractDoc :: Markdown
contractDoc = [md|
  This documentation describes a smart contract which implements
  [FA2 spec](https://gitlab.com/tzip/tzip/-/blob/1f83a3671cdff3ab4517bfa9ee5a57fc5276d4ff/proposals/tzip-12/tzip-12.md).
  |]

type Operators = BigMap (Address, Address) ()

-- Despite supporting only one token, we use a (Address, TokenId) key
-- for the ledger key, since we want to be similar to contracts that might
-- support more than one type of tokens.
type Ledger = BigMap (Address, TokenId) Natural

type LedgerInner = Map (Address, TokenId) Natural

-- | Storage of the FA2 contract
data Storage = Storage
  { sLedger        :: Ledger
  , sOperators     :: Operators
  , sTokenMetadata :: BigMap TokenId TokenMetadata
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  -- The field naming modifier is required for the metadata to show up with the
  -- required annotation as per FA2 spec.

type IsStorageC s =
  ( HasField s "sOperators" Operators
  , HasField s "sLedger" Ledger
  , IsObject s
  , HasStorage s
  )

instance TypeHasDoc Storage where
  typeDocMdDescription =
    "Contract's storage holding a big_map with all balances and the operators."

mkStorage
  :: TokenMetadata
  -> Map (Address, TokenId) Natural
  -> Operators
  -> Storage
mkStorage md_ balances operators = Storage
  { sLedger = mkBigMap balances
  , sOperators = operators
  , sTokenMetadata = mkBigMap [(theTokenId, md_)]
  }

{-# DEPRECATED defaultPermissionDescriptor
    "Pick either 'defPermissionDescriptor' (as per FA2 spec) or defTestPermissionDescriptor (old behaviour)"
    #-}
defaultPermissionDescriptor :: PermissionsDescriptor
defaultPermissionDescriptor = defTestPermissionDescriptor

-- | A default permissions descriptor value that we use for tests.
defTestPermissionDescriptor :: PermissionsDescriptor
defTestPermissionDescriptor = PermissionsDescriptor
  { pdOperator = OwnerOrOperatorTransfer
  , pdReceiver = OptionalOwnerHook
  , pdSender = OptionalOwnerHook
  , pdCustom = Nothing
  }

-- We use this parameter to closlely model how the FA2 parameter will be used
-- in a project that inheritis the FA2 paramter from this library.
data FA2SampleParameter
  = CallFA2 Parameter
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints FA2SampleParameter where
  type ParameterEntrypointsDerivation FA2SampleParameter = EpdDelegate

instance TypeHasDoc FA2SampleParameter where
  typeDocMdDescription = "Describes the parameter of FA2 sample contract."

-- | FA2 implementation configuration.
data FA2Config = FA2Config
  { cAllowedTokenIds :: [TokenId]
  , cPermissionsDescriptor :: PermissionsDescriptor
  }

instance Default FA2Config where
  def = FA2Config
    { cAllowedTokenIds = [theTokenId]
    , cPermissionsDescriptor = def
    }

fa2Contract
  :: FA2Config -> Contract FA2SampleParameter Storage ()
fa2Contract config = mkContract $ compileIndigoContract (fa2Indigo config)

fa2Indigo
  :: (HasStorage Storage, HasSideEffects, IsNotInView)
  => FA2Config
  -> Var FA2SampleParameter
  -> IndigoProcedure
fa2Indigo config param = docGroup "Sample FA2 contract" do
  contractGeneralDefault
  doc (dStorage @Storage)
  doc $ DDescription contractDoc
  entryCaseRec (Proxy @FA2EntrypointsKind) param $
    (#cCallFA2 #= fa2Handler config)
    :& RNil

fa2Handler
  :: (HasStorage Storage, HasSideEffects, IsNotInView)
  => FA2Config
  -> Var Parameter
  -> IndigoProcedure
fa2Handler config param = do
  doc $ DDescription "Handler for FA2 entrypoints."
  entryCaseSimple param
    ( #cBalance_of #= balanceOf @Storage config
    , #cTransfer #= transfer @Storage config
    , #cUpdate_operators #= updateOperators @Storage config
    )

transfer
  :: forall s. (IsStorageC s, HasSideEffects, IsNotInView)
  => FA2Config
  -> Var TransferParams
  -> IndigoProcedure
transfer config tp = do
    doc $ DDescription "Transfer tokens between accounts specified in the paramter."
    case operatorPolicy of
      NoTransfer -> failCustom_ @() #fA2_TX_DENIED
      _ -> do
        forEach tp transferItem
        executeOwnerHooks config tp
  where
    operatorPolicy = pdOperator $ cPermissionsDescriptor config

    senderCheck :: OwnerOrOperatorTransfer -> Expr Address ->  IndigoProcedure -> IndigoProcedure
    senderCheck NoTransfer _ _ = error "Impossible" -- Because we check this in main function
    senderCheck OwnerTransfer from continuation =
      if_ (from == sender) continuation (failCustom_ @() #fA2_NOT_OWNER :: IndigoProcedure)
    senderCheck OwnerOrOperatorTransfer from continuation =
      if_ (from == sender)
        continuation
        (do
           isOp <- isOperator_ @s from sender
           if_ isOp continuation (failCustom_ @() #fA2_NOT_OPERATOR :: IndigoProcedure))

    transferItem
      :: Var TransferItem
      -> IndigoProcedure
    transferItem ti = do
      let from = ti #! #tiFrom
      let txs = ti #! #tiTxs
      senderCheck operatorPolicy from $ do
        let transferOneTx
              :: Var TransferDestination
              -> IndigoProcedure
            transferOneTx td = do
              let to = td #! #tdTo
              let token_id = td #! #tdTokenId
              let amt = td #! #tdAmount
              validateTokenIds config (token_id .: nil)
              debitFrom @s from (token_id, amt)
              creditTo @s to (token_id, amt)
        forEach txs transferOneTx

withOwnerHooks
  :: OwnerTransferMode
  -> (Bool -> Expr TransferDescriptorParam -> Expr Address -> IndigoProcedure)
  -> Expr TransferDescriptorParam
  -> Expr Address
  -> IndigoProcedure
withOwnerHooks ot cb tparam addr = case ot of
  OwnerNoHook -> pass
  OptionalOwnerHook -> cb False tparam addr
  RequiredOwnerHook -> cb True tparam addr

executeOwnerHooks
  :: (HasSideEffects, IsNotInView)
  => FA2Config
  -> Var TransferParams
  -> IndigoProcedure
executeOwnerHooks config tp = forEach tp executeOwnerHook
  where
    pdesc = cPermissionsDescriptor config
    executeOwnerHook
      :: Var TransferItem
      -> IndigoProcedure
    executeOwnerHook ti = do
      let from = ti #! #tiFrom
      let mkTransferDestinationDescriptor
            :: td :~> TransferDestination
            => td
            -> IndigoFunction (Var TransferDestinationDescriptor)
          mkTransferDestinationDescriptor tdst = defFunction do
            let to = tdst #! #tdTo
            let tdd = construct @TransferDestinationDescriptor
                        ( some to
                        , tdst #! #tdTokenId
                        , tdst #! #tdAmount
                        )
            let td = construct @TransferDescriptor (some from, tdd .: nil)
            let tdp = construct @TransferDescriptorParam
                        ( td .: nil
                        , sender
                        )
            withOwnerHooks (pdReceiver pdesc) callReceiverOwnerHook tdp to
            return tdd
      txs :: Var [TransferDestinationDescriptor] <- new$ []
      forEach (ti #! #tiTxs) $ \tdst -> do
        tdd  <- mkTransferDestinationDescriptor tdst
        setVar txs (tdd .: txs)
      withOwnerHooks
        (pdSender pdesc)
        callSenderOwnerHook
        ( construct @TransferDescriptorParam
            ( construct @TransferDescriptor (some from, toExpr txs) .: nil
            , sender
            )
        )
        from

callReceiverOwnerHook
  :: (HasSideEffects, IsNotInView)
  => Bool
  -> Expr TransferDescriptorParam
  -> Expr Address
  -> IndigoProcedure
callReceiverOwnerHook throwIfUnavailable tdp receiverAddr = do
  mContract <- contractCalling @FA2OwnerHook (Call @"Tokens_received") receiverAddr
  ifSome mContract (\c -> transferTokens tdp (0 mutez) c)
    (if throwIfUnavailable then (failCustom_ @() #fA2_RECEIVER_HOOK_UNDEFINED :: IndigoProcedure) else pass)

callSenderOwnerHook
  :: (HasSideEffects, IsNotInView)
  => Bool
  -> Expr TransferDescriptorParam
  -> Expr Address
  -> IndigoProcedure
callSenderOwnerHook throwIfUnavailable tdp senderAddr = do
  mContract <- contractCalling @FA2OwnerHook (Call @"Tokens_sent") senderAddr
  ifSome mContract (\c -> transferTokens tdp (0 mutez) c)
    (if throwIfUnavailable then (failCustom_ @() #fA2_SENDER_HOOK_UNDEFINED :: IndigoProcedure) else pass)

debitFrom
  :: forall s. IsStorageC s
  => Expr Address
  -> (Expr TokenId, Expr Natural)
  -> IndigoProcedure
debitFrom from (tkId, amt) = defFunction $ do
  ledger_ <- getStorageField @s #sLedger
  let mcurrentAmount = ledger_ #: (pair from tkId)
  ifSome mcurrentAmount (\currentAmount -> do
    let newAmount = currentAmount - amt
    ifSome (isNat newAmount) (\newAmountNat -> do
      let newLedger = ledger_ !: ((pair from tkId), some newAmountNat)
      setStorageField @s #sLedger newLedger)
        (failCustom @() #fA2_INSUFFICIENT_BALANCE (pair (name #required amt) (name #present currentAmount)))
    ) (when (amt > 0 nat) $ failCustom @() #fA2_INSUFFICIENT_BALANCE (pair (name #required amt) (name #present (0 nat))))

creditTo
  :: forall s. IsStorageC s
  => Expr Address
  -> (Expr TokenId, Expr Natural)
  -> IndigoProcedure
creditTo from (tkId, amt) = do
  ledger_ <- getStorageField @s #sLedger
  let mcurrentAmount = ledger_ #: (pair from tkId)
  ifSome mcurrentAmount (\currentAmount -> do
    let newAmount = currentAmount + amt
    let newLedger = ledger_ !: ((pair from tkId), some newAmount)
    setStorageField @s #sLedger newLedger) (do
      let newLedger = ledger_ +: ((pair from tkId), amt)
      setStorageField @s #sLedger newLedger)

-- Checks if the provided address is an operator
-- for the owner address, after looking up in the ledger.
isOperator_
  :: forall s. IsStorageC s
  => Expr Address
  -> Expr Address
  -> IndigoFunction (Var Bool)
isOperator_ owner operator = do
  ret <- new$ False
  operators <- getStorageField @s #sOperators
  ifSome (operators #: (pair owner operator))
    (\_ -> setVar ret True) pass
  pure ret

balanceOf
  :: forall s. (IsStorageC s, HasSideEffects, IsNotInView)
  => FA2Config
  -> Var BalanceRequestParams
  -> IndigoProcedure
balanceOf config br = do
  doc $ DDescription "Get balance of the accounts specified in the parameter."

  ledger_ <- getStorageField @s #sLedger

  project (coerce @(View_ [BalanceRequestItem] [BalanceResponseItem])  br) $ \reqs -> do
    result <- new$ ([] :: [BalanceResponseItem])
    token_ids <- new$ ([] :: [TokenId])
    forEach reqs $ \req -> do
      setVar token_ids ((req #! #briTokenId) .: token_ids)
      let mEntry = ledger_ #: (pair (req #! #briOwner) (req #! #briTokenId))
      ifSome mEntry
        (\entry ->
          setVar result (construct @BalanceResponseItem (toExpr req, toExpr entry) .: result)
        )
        (setVar result (construct @BalanceResponseItem (toExpr req, toExpr (0 nat)) .: result))
    validateTokenIds config token_ids
    reversed <- reverseList result
    pure (varExpr reversed)

reverseList :: NiceConstant a => Var [a] -> IndigoFunction (Var [a])
reverseList inp = do
  result <- new$ ([] :: [a])
  forEach inp $ \a -> setVar result (a .: result)
  pure result

validateTokenIds
  :: (exp :~> [TokenId])
  => FA2Config
  -> exp
  -> IndigoProcedure
validateTokenIds config tokenIds =
  forEach tokenIds $ \tokenId -> do
    isAllowed <- new$ False
    forEach (cAllowedTokenIds config) $ \allowedTokenId ->
      isAllowed ||= (tokenId == allowedTokenId)
    unless isAllowed
      (failCustom_ @() #fA2_TOKEN_UNDEFINED)

permissionsDescriptor
  :: (HasSideEffects, IsNotInView)
  => FA2Config
  -> Var PermissionsDescriptorParam
  -> IndigoProcedure
permissionsDescriptor config cref = do
  permissionDescriptor <- new$ cPermissionsDescriptor config
  transferTokens permissionDescriptor amount cref

updateOperators
  :: forall s. IsStorageC s
  => FA2Config
  -> Var UpdateOperatorsParam
  -> IndigoProcedure
updateOperators config upds = do
  doc $ DDescription "Update operators for an accounts."
  let
    withTransferModeCheck :: IndigoProcedure -> IndigoProcedure
    withTransferModeCheck continuation = do
      case pdOperator $ cPermissionsDescriptor config of
        NoTransfer ->
            (failCustom_ @() #fA2_OPERATORS_UNSUPPORTED)
        OwnerTransfer ->
            (failCustom_ @() #fA2_OPERATORS_UNSUPPORTED)
        OwnerOrOperatorTransfer -> continuation

  withTransferModeCheck $ forEach upds $ \upd -> do
    case_ upd
      ( #cAddOperator #= (\op -> addOperator @s config op)
      , #cRemoveOperator #= (\op -> removeOperator @s config op)
      )

addOperator
  :: forall s. IsStorageC s
  => FA2Config
  -> Var OperatorParam
  -> IndigoProcedure
addOperator config op = do
  let owner = op #! #opOwner
  let operator = op #! #opOperator
  validateTokenIds config (op #! #opTokenId .: nil)
  assertCustom_ #nOT_OWNER (owner == sender)
  operators <- getStorageField @s #sOperators
  let isOpPresent = operators #: (pair owner operator)
  ifSome isOpPresent
   (const pass)
   (setStorageField @s #sOperators (operators +: ((pair owner operator), ()) ))

removeOperator
  :: forall s. IsStorageC s
  => FA2Config
  -> Var OperatorParam
  -> IndigoProcedure
removeOperator config op = do
  let owner = op #! #opOwner
  let operator = op #! #opOperator
  validateTokenIds config (op #! #opTokenId .: nil)
  assertCustom_ #nOT_OWNER (owner == sender)
  operators <- getStorageField @s #sOperators
  let isOpPresent = operators #: (pair owner operator)
  ifSome isOpPresent
   (\_ -> setStorageField @s #sOperators (operators -: (pair owner operator)) )
   pass

type instance ErrorArg "nOT_OWNER" = ()

instance CustomErrorHasDoc "nOT_OWNER" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "The sender of transaction is not owner"
