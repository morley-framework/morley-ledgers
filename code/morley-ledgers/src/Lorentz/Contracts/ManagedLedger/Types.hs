-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Lorentz.Contracts.ManagedLedger.Types
  ( Storage
  , StorageRPC
  , StorageC
  , StorageSkeleton (..)
  , StorageSkeletonRPC (..)
  , LedgerC
  , LedgerValue
  , mkStorage
  , mkStorageSkeleton
  , storageSkeletonNotes
  , storageNotes
  ) where

import Indigo as I (HasField(..), fieldLensADT, fieldLensDeeper)
import Lorentz
import Prelude (sum, (<$>))

import Fmt (Buildable(..))

import Lorentz.Contracts.Spec.ApprovableLedgerInterface (GetAllowanceParams)
import Morley.AsRPC (HasRPCRepr(..), deriveRPC)
import Morley.Michelson.Typed (Notes(..))
import Morley.Michelson.Untyped (annQ, noAnn)
import Morley.Util.Named
import Morley.Util.TypeLits (Symbol)
import Test.Cleveland.Instances ()

----------------------------------------------------------------------------
-- Storage
----------------------------------------------------------------------------

type LedgerValue = "balance" :! Natural

data StorageSkeleton fields = StorageSkeleton
  { ledger    :: BigMap Address LedgerValue
  , approvals :: BigMap GetAllowanceParams Natural
  , fields    :: fields
  } deriving stock (Generic, Eq)
    deriving anyclass (IsoValue, HasAnnotation)
    deriving anyclass Buildable

instance (StoreHasField fields (fname :: Symbol) ftype, IsoValue fields, Dupable fields) =>
    StoreHasField (StorageSkeleton fields) fname ftype where
  storeFieldOps = storeFieldOpsDeeper #fields

instance {-# OVERLAPPING #-} (IsoValue fields, Dupable fields) =>
    StoreHasSubmap (StorageSkeleton fields) "ledger" Address LedgerValue where
  storeSubmapOps = storeSubmapOpsDeeper #ledger

instance {-# OVERLAPPING #-} (IsoValue fields, Dupable fields) =>
    StoreHasSubmap (StorageSkeleton fields) "approvals" GetAllowanceParams Natural where
  storeSubmapOps = storeSubmapOpsDeeper #approvals

-- | Create a default storage with ability to set some balances to
-- non-zero values.
mkStorageSkeleton :: Map Address Natural -> fields -> StorageSkeleton fields
mkStorageSkeleton balances flds = StorageSkeleton
  { ledger    = mkBigMap $ toLedgerValue <$> balances
  , approvals = mkBigMap (mempty :: [(GetAllowanceParams, Natural)])
  , fields    = flds
  }
  where
    toLedgerValue initBal = #balance :! initBal

-- | A temporary approach to add field annotations to 'StorageSkeleton'.
storageSkeletonNotes :: Notes (ToT fields) -> Notes (ToT (StorageSkeleton fields))
storageSkeletonNotes fieldNotes = NTPair noAnn [annQ|ledger|] noAnn noAnn noAnn
  (NTBigMap noAnn (NTAddress [annQ|user|]) (NTNat [annQ|balance|]))
  (NTPair noAnn [annQ|approvals|] noAnn noAnn noAnn
    (NTBigMap noAnn
      (NTPair noAnn noAnn noAnn noAnn noAnn
        (NTAddress [annQ|owner|])
        (NTAddress [annQ|spender|])
      )
      (NTNat [annQ|value|])
    )
    fieldNotes
  )

data StorageFields = StorageFields
  { admin       :: Address
  , paused      :: Bool
  , totalSupply :: Natural
  } deriving stock (Generic, Eq, Show)
    deriving anyclass (IsoValue, HasAnnotation)
    deriving anyclass Buildable

instance HasRPCRepr StorageFields where
  type AsRPC StorageFields = StorageFields

instance TypeHasDoc StorageFields where
  typeDocMdDescription = "Managed ledger storage fields."

  typeDocHaskellRep = homomorphicTypeDocHaskellRep
  typeDocMichelsonRep = homomorphicTypeDocMichelsonRep

instance HasFieldOfType StorageFields name field =>
         StoreHasField StorageFields name field where
  storeFieldOps = storeFieldOpsADT

deriveRPC "StorageSkeleton"

type Storage = StorageSkeleton StorageFields

type StorageRPC = StorageSkeletonRPC StorageFields

instance TypeHasDoc a => TypeHasDoc (StorageSkeleton a) where
  typeDocMdDescription = "Managed ledger storage skeleton."

  typeDocMdReference = poly1TypeDocMdReference
  typeDocHaskellRep = concreteTypeDocHaskellRep @(StorageSkeleton StorageFields)
  typeDocMichelsonRep = concreteTypeDocMichelsonRep @(StorageSkeleton StorageFields)

----------------------------------------------------------------------------
-- HasField instances for Indigo version
----------------------------------------------------------------------------

instance I.HasField Storage "ledger" (BigMap Address LedgerValue) where
  fieldLens = fieldLensADT #ledger

instance I.HasField Storage "approvals" (BigMap GetAllowanceParams Natural) where
  fieldLens = fieldLensADT #approvals

instance I.HasField StorageFields "admin" Address where
  fieldLens = fieldLensADT #admin

instance I.HasField StorageFields "paused" Bool where
  fieldLens = fieldLensADT #paused

instance I.HasField StorageFields "totalSupply" Natural where
  fieldLens = fieldLensADT #totalSupply

instance I.HasField Storage "admin" Address where
  fieldLens = fieldLensDeeper #fields

instance I.HasField Storage "paused" Bool where
  fieldLens = fieldLensDeeper #fields

instance I.HasField Storage "totalSupply" Natural where
  fieldLens = fieldLensDeeper #fields

-- | Create a default storage with ability to set some balances to
-- non-zero values.
mkStorage :: Address -> Map Address Natural -> Storage
mkStorage adminAddress balances = mkStorageSkeleton balances $
  StorageFields
  { admin = adminAddress
  , paused = False
  , totalSupply = sum balances
  }

type StorageC store =
  ( LedgerC store
  , StorageContains store
   [ "admin" := Address
   , "paused" := Bool
   ]
  )

type LedgerC store =
  ( Dupable store
  , StorageContains store
    [ "totalSupply" := Natural
    , "ledger" := Address ~> LedgerValue
    , "approvals" := GetAllowanceParams ~> Natural
    ]
  )

-- | A temporary approach to add field annotations to 'Storage'.
storageNotes :: Notes (ToT Storage)
storageNotes = storageSkeletonNotes @StorageFields $
  (NTPair noAnn noAnn noAnn noAnn noAnn
    (NTAddress [annQ|admin|])
    (NTPair noAnn noAnn noAnn noAnn noAnn
    (NTBool [annQ|paused|]) (NTNat [annQ|totalSupply|]))
  )
