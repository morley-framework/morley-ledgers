-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Managed ledger which is compatible with FA1.2 standard and
-- extended with administrator functionality.

module Lorentz.Contracts.ManagedLedger
    ( Parameter(..)

    , Storage
    , StorageRPC
    , StorageSkeletonRPC(..)
    , StorageC
    , LedgerValue
    , mkStorage
    , storageNotes

    , managedLedgerContract
    ) where

import Lorentz.Contracts.ManagedLedger.Impl
import Lorentz.Contracts.ManagedLedger.Types
import Lorentz.Contracts.Spec.ManagedLedgerInterface
