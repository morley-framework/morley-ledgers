-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA
{-# OPTIONS_GHC -Wno-orphans #-}

-- | FA2 interface specification.
-- https://gitlab.com/tzip/tzip/-/blob/1f83a3671cdff3ab4517bfa9ee5a57fc5276d4ff/proposals/tzip-12/tzip-12.md
--
module Lorentz.Contracts.Spec.FA2Interface
  ( BalanceRequestItem(..)
  , BalanceRequestParams
  , BalanceResponseItem(..)
  , FA2OwnerHook (..)
  , FA2EntrypointsKind
  , FA2View (..)
  , mkFA2View
  , OperatorParam(..)
  , OwnerOrOperatorTransfer (..)
  , OwnerTransferMode (..)
  , Parameter (..)
  , ParameterC
  , PermissionsDescriptor (..)
  , defPermissionDescriptor
  , PermissionsDescriptorParam
  , TokenId (..)
  , theTokenId
  , TokenMetadata
  , mkTokenMetadata
  , TransferDescriptor(..)
  , TransferDescriptorParam(..)
  , TransferDestination(..)
  , TransferDestinationDescriptor(..)
  , TransferItem(..)
  , TransferParams
  , UpdateOperator (..)
  , UpdateOperatorsParam
  , FA2FieldNamingStragegy
  ) where

import Lorentz
import Prelude (unsafe)

import Data.Map qualified as Map
import Data.Text.Encoding (encodeUtf8)
import Fmt (Buildable(..), (+|), (|+))

import Morley.Michelson.Typed
import Morley.Michelson.Untyped
import Morley.Util.TypeLits

----------------------------------------------------------------------------
-- Helpers
----------------------------------------------------------------------------

-- | Type marker for deriving via.
data FA2FieldNamingStragegy

-- | For records.
instance TypeHasFieldNamingStrategy FA2FieldNamingStragegy where
  typeFieldNamingStrategy = appendTo "_" ["to", "from"] . toSnake . dropPrefix

----------------------------------------------------------------------------
-- FA2
----------------------------------------------------------------------------

newtype TokenId = TokenId { unTokenId :: Natural }
  deriving stock (Generic, Eq, Ord, Show)
  deriving newtype (IsoValue, HasAnnotation, Buildable)

instance TypeHasDoc TokenId where
  typeDocMdDescription =
    "Token identifier as defined by [TZIP-12](https://gitlab.com/tzip/tzip/-/blob/1f83a3671cdff3ab4517bfa9ee5a57fc5276d4ff/proposals/tzip-12/tzip-12.md#general)."

-- | If your contract has only one type of token, you have to use this value
-- as its identifier.
theTokenId :: TokenId
theTokenId = TokenId 0

data TransferDestination = TransferDestination
  { tdTo :: Address
  , tdTokenId :: TokenId
  , tdAmount :: Natural
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  deriving anyclass Buildable

instance TypeHasDoc TransferDestination where
  typeDocMdDescription = "Describes the amount of tokens to transfer and to whom"

data TransferItem = TransferItem
  { tiFrom :: Address
  , tiTxs :: [TransferDestination]
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  deriving anyclass Buildable

instance TypeHasDoc TransferItem where
  typeDocMdDescription = "Describes a transfer operation"

type TransferParams = [TransferItem]

data BalanceRequestItem = BalanceRequestItem
  { briOwner :: Address
  , briTokenId :: TokenId
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  deriving anyclass Buildable

instance TypeHasDoc BalanceRequestItem where
  typeDocMdDescription = "Describes a request for an owner's balance"

data BalanceResponseItem = BalanceResponseItem
  { briRequest :: BalanceRequestItem
  , briBalance :: Natural
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  deriving anyclass Buildable

instance TypeHasDoc BalanceResponseItem where
  typeDocMdDescription = "Describes a response to a request for an owner's balance"

-- We use a Symbol phantom type parameter here so that the list argument will
-- show up with the provided field annotation, owing to the custom HasAnnotation
-- instance for FA2View.
newtype FA2View (paramFa :: Symbol) a r = FA2View (View_ a r)
  deriving newtype (Eq, Show, Generic, TypeHasDoc)

deriving newtype instance IsoValue (View_ a r) => IsoValue (FA2View paramFa a r)

instance Buildable (View_ a r) => Buildable (FA2View paramFa a r) where
  build (FA2View v) = build v

instance CanCastTo (FA2View paramFa a r) (View_ a r)

instance (HasAnnotation a, HasAnnotation r, KnownSymbol paramFa)
  => HasAnnotation (FA2View paramFa a r) where
  getAnnotation fe = case getAnnotation @(View_ a r) fe  of
      (NTPair t1 _ _ vn1 vn2 cn1 cn2) ->
        (NTPair t1 (unsafe $ mkAnnotation $ symbolValT' @paramFa)
          [annQ|callback|] vn1 vn2 cn1 (cn2 :: Notes ('Morley.Michelson.Typed.TContract (ToT r))))

-- | Polymorphic constructor for 'FA2View'.
mkFA2View :: ToContractRef r contract => a -> contract -> FA2View paramFa a r
mkFA2View a c = FA2View $ mkView_ a c

type BalanceRequestParams = FA2View "requests" [BalanceRequestItem] [BalanceResponseItem]

type TokenMetadata = Map MText ByteString

mkTokenMetadata ::  Text -> Text -> Text -> TokenMetadata
mkTokenMetadata symbol name decimals = Map.fromList
  [ ([mt|symbol|], encodeUtf8 symbol)
  , ([mt|name|], encodeUtf8 name)
  , ([mt|decimals|], encodeUtf8 decimals)
  ]

data OwnerOrOperatorTransfer
  = NoTransfer
  | OwnerTransfer
  | OwnerOrOperatorTransfer
  deriving stock (Generic, Eq, Show)
  deriving anyclass IsoValue

instance HasAnnotation OwnerOrOperatorTransfer where
  annOptions = Just def { fieldAnnModifier = toSnake }

instance TypeHasDoc OwnerOrOperatorTransfer where
  typeDocMdDescription = "Describes if owner or operator can make a transfer"

data OwnerTransferMode
  = OwnerNoHook
  | OptionalOwnerHook
  | RequiredOwnerHook
  deriving stock (Generic, Eq, Show)
  deriving anyclass IsoValue

instance HasAnnotation OwnerTransferMode where
  annOptions = Just def { fieldAnnModifier = toSnake }

instance TypeHasDoc OwnerTransferMode where
  typeDocMdDescription = "Describes if only owner is allowed to make the transfer"

data CustomPermissionPolicy = CustomPermissionPolicy
  { cppTag :: MText
  , cppConfigApi :: Maybe Address
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy

instance TypeHasDoc CustomPermissionPolicy where
  typeDocMdDescription = "Describes a custom behavior that extends the default transfer permission policy"

data PermissionsDescriptor = PermissionsDescriptor
  { pdOperator :: OwnerOrOperatorTransfer
  , pdReceiver :: OwnerTransferMode
  , pdSender :: OwnerTransferMode
  , pdCustom :: Maybe CustomPermissionPolicy
  }

$(customGeneric "PermissionsDescriptor" rightComb)

deriving stock instance Show PermissionsDescriptor
deriving anyclass instance IsoValue PermissionsDescriptor
deriving anyclass instance HasAnnotation PermissionsDescriptor
deriving via FA2FieldNamingStragegy instance TypeHasFieldNamingStrategy PermissionsDescriptor

instance TypeHasDoc PermissionsDescriptor where
  typeDocMdDescription =
    "Contract's storage holding a big_map with all balances and the operators."
  typeDocHaskellRep = homomorphicTypeDocHaskellRep

instance Default PermissionsDescriptor where
  def = defPermissionDescriptor

-- | The default permissions descriptor value.
-- In case a contract uses these permissions, it can save itself from
-- having to implement the permission descriptor entrypoint.
--
-- See also <https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/permissions-policy.md permissions spec>
-- (each type mentions its default value).
defPermissionDescriptor :: PermissionsDescriptor
defPermissionDescriptor = PermissionsDescriptor
  { pdOperator = OwnerOrOperatorTransfer
  , pdReceiver = OwnerNoHook
  , pdSender = OwnerNoHook
  , pdCustom = Nothing
  }

type PermissionsDescriptorParam = ContractRef PermissionsDescriptor

data OperatorParam = OperatorParam
  { opOwner :: Address
  , opOperator :: Address
  , opTokenId :: TokenId
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  deriving anyclass Buildable

instance TypeHasDoc OperatorParam where
  typeDocMdDescription = "Describes an address authorized to transfer tokens on behalf of a token owner"

data UpdateOperator
  = AddOperator OperatorParam
  | RemoveOperator OperatorParam
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue)
  deriving anyclass Buildable

instance HasAnnotation UpdateOperator where
  annOptions = Just def { fieldAnnModifier = toSnake }

instance TypeHasDoc UpdateOperator where
  typeDocMdDescription = "Describes the operator update operation."

type UpdateOperatorsParam = [UpdateOperator]

data Parameter
  = Balance_of BalanceRequestParams
  | Transfer TransferParams
  | Update_operators UpdateOperatorsParam
  deriving stock (Eq, Show)

instance Buildable Parameter where
  build = \case
    Balance_of p -> "<Balance_of:" +| build p |+ ">"
    Transfer p -> "<Transfer:" +| build p |+ ">"
    Update_operators p -> "<Update_operators:" +| build p |+ ">"

type ParameterC param =
  ParameterContainsEntrypoints param
    [ "Transfer" :> TransferParams
    , "Balance_of" :> BalanceRequestParams
    , "Update_operators" :> UpdateOperatorsParam
    ]

-- | Owner hook interface
--
data TransferDestinationDescriptor = TransferDestinationDescriptor
  { tddTo :: Maybe Address
  , tddTokenId :: TokenId
  , tddAmount :: Natural
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  deriving anyclass Buildable

data TransferDescriptor = TransferDescriptor
  { tdFrom :: Maybe Address
  , tdTxs :: [TransferDestinationDescriptor]
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  deriving anyclass Buildable

data TransferDescriptorParam = TransferDescriptorParam
  { tdpBatch :: [TransferDescriptor]
  , tdpOperator :: Address
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FA2FieldNamingStragegy
  deriving anyclass Buildable

data FA2OwnerHook
  = Tokens_sent TransferDescriptorParam
  | Tokens_received TransferDescriptorParam
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue, HasAnnotation)
  deriving anyclass Buildable

data FA2EntrypointsKind

instance EntrypointKindHasDoc FA2EntrypointsKind where
  entrypointKindPos = 1001
  entrypointKindSectionName = "FA2EntrypointsKind"

instance ParameterHasEntrypoints FA2OwnerHook where
  type ParameterEntrypointsDerivation FA2OwnerHook = EpdPlain

--------------------------------------------------------------------------------
-- Errors
--------------------------------------------------------------------------------

type instance ErrorArg "fA2_INSUFFICIENT_BALANCE" = ("required" :! Natural, "present" :! Natural)

instance CustomErrorHasDoc "fA2_INSUFFICIENT_BALANCE" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "The source of a transfer did not contain sufficient tokens"

type instance ErrorArg "fA2_NOT_OWNER" = ()

instance CustomErrorHasDoc "fA2_NOT_OWNER" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "The sender of transfer is not the owner of tokens and operator transfer is disabled"

type instance ErrorArg "fA2_NOT_OPERATOR" = ()

instance CustomErrorHasDoc "fA2_NOT_OPERATOR" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "The sender of transfer is not the owner or the authorized operator"

type instance ErrorArg "fA2_TX_DENIED" = ()

instance CustomErrorHasDoc "fA2_TX_DENIED" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Transfers are disabled"

type instance ErrorArg "fA2_RECEIVER_HOOK_UNDEFINED" = ()

instance CustomErrorHasDoc "fA2_RECEIVER_HOOK_UNDEFINED" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Transfer hooks were undefined on the reciever address"

type instance ErrorArg "fA2_SENDER_HOOK_UNDEFINED" = ()

instance CustomErrorHasDoc "fA2_SENDER_HOOK_UNDEFINED" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Transfer hooks were undefined on the sender address"

type instance ErrorArg "fA2_TOKEN_UNDEFINED" = ()

instance CustomErrorHasDoc "fA2_TOKEN_UNDEFINED" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Contract received an unsupported token id"

type instance ErrorArg "fA2_OPERATORS_UNSUPPORTED" = ()

instance CustomErrorHasDoc "fA2_OPERATORS_UNSUPPORTED" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Operators are forbidden in permissions descriptor and update operators operation was requested"
