-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
{-# OPTIONS_GHC -Wno-orphans #-}

-- | FA2 interface specification.
-- https://gitlab.com/tzip/tzip/-/blob/1f83a3671cdff3ab4517bfa9ee5a57fc5276d4ff/proposals/tzip-12/tzip-12.md
--
-- These instances are in a separate module because sometimes, our contract's FA2
-- parameter will have a different top level structure (FA2 only specifies
-- the structure of individual entrypoints), and thus we might want to
-- implement/define a custom Michelson representation (maybe using
-- `customGeneric` infra), and thus might not want to import these instance by
-- default.
module Lorentz.Contracts.Spec.FA2Interface.ParameterInstances () where

import Lorentz

import Lorentz.Contracts.Spec.FA2Interface

deriving stock instance Generic Parameter
deriving anyclass instance HasAnnotation Parameter
deriving anyclass instance IsoValue Parameter

instance ParameterHasEntrypoints Parameter where
  type ParameterEntrypointsDerivation Parameter = EpdPlain

instance TypeHasDoc Parameter where
  typeDocMdDescription = "Describes the FA2 operations."
