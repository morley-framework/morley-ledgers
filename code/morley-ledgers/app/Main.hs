-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE ApplicativeDo #-}

module Main
  ( main
  ) where

import Data.Map qualified as Map
import Data.Version (showVersion)
import Main.Utf8 (withUtf8)
import Options.Applicative qualified as Opt
import Options.Applicative.Help.Pretty (Doc, linebreak)
import Paths_morley_ledgers (version)

import Indigo.Contracts.AbstractLedger (abstractLedgerContract)
import Indigo.Contracts.AbstractLedger qualified as AL
import Indigo.Contracts.FA2Sample as FA2Sample
import Lorentz (DGitRevision, GitRepoSettings(..), def, mkDGitRevision)
import Lorentz.ContractRegistry
import Lorentz.Contracts.ManagedLedger (managedLedgerContract)
import Lorentz.Contracts.ManagedLedger qualified as ML
import Lorentz.Contracts.Spec.FA2Interface as FA2
import Morley.Tezos.Address.Kinds
import Morley.CLI (addressOption)
import Morley.Util.CLI (mkCLOptionParser)
import Morley.Util.Named
import Lorentz.Address (ToAddress(..))

repoSettings :: GitRepoSettings
repoSettings = GitRepoSettings $ \commit ->
  "https://gitlab.com/morley-framework/morley-ledgers/-/tree/" <> commit

programInfo :: DGitRevision -> Opt.ParserInfo CmdLnArgs
programInfo gitRev = Opt.info (Opt.helper <*> versionOption <*> argParser contracts gitRev) $
  mconcat
  [ Opt.fullDesc
  , Opt.progDesc "Ledger contracts registry"
  , Opt.header "Ledger contracts for Michelson"
  , Opt.footerDoc usageDoc
  ]
  where
    versionOption = Opt.infoOption ("morley-ledgers-" <> showVersion version)
      (Opt.long "version" <> Opt.help "Show version.")

usageDoc :: Maybe Doc
usageDoc = Just $ mconcat
   [ "You can use help for specific COMMAND", linebreak
   , "EXAMPLE:", linebreak
   , "  morley-ledgers print --help", linebreak
   ]

contracts :: ContractRegistry
contracts = ContractRegistry $ Map.fromList
  [ "ManagedLedger" ?:: ContractInfo
    { ciContract = managedLedgerContract
    , ciIsDocumented = True
    , ciStorageParser = Just mlStorageParser
    , ciStorageNotes = Nothing
    }
  , "AbstractLedger" ?:: ContractInfo
    { ciContract = abstractLedgerContract
    , ciIsDocumented = True
    , ciStorageParser = Just alStorageParser
    , ciStorageNotes = Nothing
    }
  , "FA2Sample" ?:: ContractInfo
    { ciContract = fa2Contract def
    , ciIsDocumented = True
    , ciStorageParser = Just fa2SampleStorageParser
    , ciStorageNotes = Nothing
    }
  ]

mlStorageParser :: Opt.Parser ML.Storage
mlStorageParser = do
  admin <-
    toAddress <$> addressOption @'AddressKindImplicit Nothing (#name :! "admin")
    (#help :! "Initial administrator address")
  -- Usually we don't have any balances initially.
  pure (ML.mkStorage admin mempty)

alStorageParser :: Opt.Parser AL.Storage
alStorageParser = pure AL.emptyStorage

fa2SampleStorageParser :: Opt.Parser FA2Sample.Storage
fa2SampleStorageParser = do
  mdSymbol <-
    mkCLOptionParser @Text Nothing (#name :! "symbol")
    (#help :! "Token symbol for metadata")
  mdName <-
    mkCLOptionParser @Text Nothing (#name :! "name")
    (#help :! "Token name for metadata")
  pure $ FA2Sample.mkStorage (mkTokenMetadata mdSymbol mdName "8") mempty mempty

main :: IO ()
main = withUtf8 $ do
  let gitRev = $(mkDGitRevision) repoSettings
  cmdLnArgs <- Opt.execParser (programInfo gitRev)
  runContractRegistry contracts cmdLnArgs `catchAny` (die . displayException)
