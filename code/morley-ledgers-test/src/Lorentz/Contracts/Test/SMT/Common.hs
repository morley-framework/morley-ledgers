-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Common types and function for State Machine Tests

{-# OPTIONS_GHC -Wno-orphans #-}

module Lorentz.Contracts.Test.SMT.Common
  ( -- * Fixture and Engine State
    Fixture (..)
  , EngineState (..)
  , esIsErrored

  -- * Address representation
  , Addr (..)
  , adminAddr
  , adminAddress
  , addrPool
  , addressPool
  , addrInPool
  , addressInPool

  -- * Generators
  , anyAddr
  , genFixture
  , genInitFixture
  , anyBalance
  , maxGenBalance

  -- * Validation
  , validateFinalState
  , validateViewResult

  -- * Utils
  , continueWith
  , withAddrSender
  , anyFires
  , elementOr
  , withFreq
  ) where

import Data.List (elemIndex)
import Fmt (Buildable(..), blockListF, listF', nameF, pretty, (+|), (|+))
import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Text.Show qualified
import Unsafe ((!!))

import Lorentz.Contracts.Test.ApprovableLedger (AlSettings(..))
import Morley.Michelson.Runtime.GState (genesisAddresses)
import Morley.Michelson.Typed (SomeConstant)
import Test.Cleveland hiding (scenario)
import Test.Cleveland.Internal.Actions
import Morley.Tezos.Address.Alias (Alias(..))

-- Orphan instance that we need to use HashMaps in Engine State
instance (Buildable k, Buildable v) => Buildable (HashMap k v) where
  build = listF' (\(k, v) -> k |+ ": " +| v |+ "") . toPairs

----------------------------------------------------------------------------
-- Fixture and Engine State
----------------------------------------------------------------------------

-- | Describes test scenario.
--
-- Invariant: this scenario should not result in error if this error type is not
-- related the tested interface.
data Fixture action progressState = Fixture
  { fActions :: [action]
  , fFinalState :: EngineState progressState
  }

instance (Buildable action, Buildable progressState) =>
    Show (Fixture action progressState) where
  show = pretty

instance (Buildable action, Buildable progressState) =>
    Buildable (Fixture action progressState) where
  build (Fixture actions es) =
    nameF "Generated actions" (blockListF actions) <> "\n" <>
    "Expected state: " <> build es

data EngineState progressState
  = EsInProgress progressState
    -- ^ Everything is ok and we can possibly continue scenario.
  | EsErrored (NonEmpty SomeConstant)
    -- ^ At this point one of the given errors should occur.
    -- In the contract we are not oblidged to ensure any specific validation
    -- order, so it's better not to expect only one specific error in these
    -- tests as well.

instance Buildable progressState => Buildable (EngineState progressState) where
  build = \case
    EsInProgress es ->
      "State: " <> build es
    EsErrored errs ->
      "Error state: " <> case errs of
        err :| [] -> build err
        _ -> blockListF (toList errs)

esIsErrored :: EngineState progressState -> Bool
esIsErrored = \case
  EsInProgress{} -> False
  EsErrored{} -> True

----------------------------------------------------------------------------
-- Address representation
----------------------------------------------------------------------------

newtype Addr = Addr Word
  deriving stock (Show, Eq, Ord)
  deriving newtype (Hashable)

instance Buildable Addr where
  build (Addr i) = "#" <> build i

adminAddr :: Addr
adminAddr = addrInPool adminAddress

adminAddress :: ImplicitAddressWithAlias
adminAddress = addressPool Unsafe.!! 0

addressPoolSize :: Word
addressPoolSize = fromIntegralOverflowing $ length addressPool

-- | Pool of 'Addr' used in the test, each is associated with the 'Address' in
-- the 'addressPool' in the same position.
addrPool :: [Addr]
addrPool = Addr <$> [0 .. addressPoolSize - 1]

-- | Pool of 'ImplicitAddressWithAlias' used in the tests.
addressPool :: [ImplicitAddressWithAlias]
addressPool = zipWith assignName (toList genesisAddresses) [(1 :: Word)..]
  where
    assignName addr n = AddressWithAlias addr $ ImplicitAlias $ "genesis_" <> show n

-- | Retrive the 'Addr' associated with the given 'ImplicitAddressWithAlias'.
addrInPool :: HasCallStack => ImplicitAddressWithAlias -> Addr
addrInPool address =
  maybe (error "Address not in pool") (Addr . fromIntegralOverflowing) $
    elemIndex address addressPool

-- | Retrive the 'ImplicitAddressWithAlias' associated with the given 'Addr'.
addressInPool :: HasCallStack => Addr -> ImplicitAddressWithAlias
addressInPool (Addr n)
  | n >= addressPoolSize =
      error $ "Wrong element in address pool: " <> pretty n
  | otherwise =
      addressPool Unsafe.!! fromIntegralOverflowing n

----------------------------------------------------------------------------
-- Generators
----------------------------------------------------------------------------

-- | Generator for a random 'Addr' from the pool
anyAddr :: MonadGen m => m Addr
anyAddr = Gen.element addrPool

-- | Generator of initial settings for the contract, 'AlSettings'
genInitSetting :: MonadGen m => m AlSettings
genInitSetting = do
  initAddrs <- forM addressPool $ \address -> do
    amount <- Gen.enum 1 maxGenBalance
    return (address, amount)
  return $ AlInitAddresses initAddrs

-- | Generates a 'Fixture' from a starting state and an actions generator
genFixture
  :: MonadGen m
  => EngineState progressState
  -> (progressState -> m (action, EngineState progressState))
  -> m (Fixture action progressState)
genFixture startEngineState genAction = uncurry Fixture <$> genActions
  where
    genActions = Gen.sized $ \(Range.Size n) ->
      Gen.enum 0 n >>= genActionsRec startEngineState genAction

-- | Generates initial settings for the contract and the fixture based on them.
-- See `genInitSetting` and `genFixture`.
genInitFixture
  :: MonadGen m
  => (AlSettings -> EngineState progressState)
  -> (progressState -> m (action, EngineState progressState))
  -> m (AlSettings, Fixture action progressState)
genInitFixture initSettingsToEngineState genAction = do
  initSettings <- genInitSetting
  alFixture <- genFixture (initSettingsToEngineState initSettings) genAction
  return (initSettings, alFixture)

genActionsRec
  :: MonadGen m
  => EngineState progressState
  -> (progressState -> m (action, EngineState progressState))
  -> Int
  -> m ([action], EngineState progressState)
genActionsRec es genAction k = case (es, k) of
  (EsErrored _, _) -> pure ([], es)
  (_, 0) -> pure ([], es)
  (EsInProgress esip, _) -> do
    (action, es') <- genAction esip
    first (action :) <$> genActionsRec es' genAction (k - 1)

-- | Generator for a random balance between 0 and 'maxGenBalance'.
anyBalance :: MonadGen m => m Natural
anyBalance = Gen.enum 0 maxGenBalance

-- | Value used as the top limit for 'anyBalance', currently 1000.
-- This value is arbitrary and this function exists only for convenience.
maxGenBalance :: Natural
maxGenBalance = 1000

----------------------------------------------------------------------------
-- Validation
----------------------------------------------------------------------------

-- | Validator for the final 'EngineState'
validateFinalState
  :: (MonadEmulated caps m)
  => EngineState progressState
  -> m a
  -> m ()
validateFinalState es scenario = case es of
  EsInProgress _ -> void scenario
  EsErrored errs -> (OrPredicate (failedWith <$> errs)) `expectTransferFailure` scenario

-- | Validator for a "view" call, needs the address of the consumer contract
validateViewResult
  :: ( Eq [res], Buildable [res], MonadEmulated caps m)
  => Either (NonEmpty SomeConstant) res
  -> ContractHandle res [res] ()
  -> m a
  -> m ()
validateViewResult expected consumer scenario = case expected of
  Right res -> scenario >> (getFullStorage consumer @@== [res])
  Left errs -> (OrPredicate (failedWith <$> errs)) `expectTransferFailure` scenario

----------------------------------------------------------------------------
-- Utils
----------------------------------------------------------------------------

-- | In-progress continuation helper for 'EngineState' that takes a "progressState"
-- and a modificator for it.
continueWith :: progressState -> State progressState a -> EngineState progressState
continueWith es modificator = EsInProgress $ execState modificator es

-- | Utility function to use 'withSender' with 'Addr'
withAddrSender :: MonadEmulated caps m => Addr -> m a -> m a
withAddrSender = withSender . addressInPool

-- | Returns the nonempty list of errors for which the predicates hold, or 'Nothing'.
anyFires :: [(Bool, SomeConstant)] -> Maybe (NonEmpty SomeConstant)
anyFires = nonEmpty . map snd . filter fst

-- | 'Gen.element' but with a default value in case the list is empty
elementOr :: MonadGen m => a -> [a] -> m a
elementOr ele [] = pure ele
elementOr _ lst = Gen.element lst

-- | Utility function to wrap a generator with its frequency to be used in a list
-- consumed by 'Gen.frequency'
withFreq :: Int -> m a -> (Int, m a)
withFreq n g = (n, g)
