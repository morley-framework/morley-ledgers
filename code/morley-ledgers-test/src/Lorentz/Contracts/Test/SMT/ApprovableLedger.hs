-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | State Machine Tests implementation for contracts satisfying the interface
-- of 'ApprovableLedger'

module Lorentz.Contracts.Test.SMT.ApprovableLedger
  ( approvableLedgerSMT

  , AApproveParams (..)
  , ATransferParams (..)
  ) where

import Control.Lens (at, makeLenses, non, (+=), (-=), (.=))
import Fmt (Buildable(..), pretty)
import Hedgehog (MonadGen, discard, forAll, property, withDiscards, withTests)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import Text.Show qualified

import Lorentz.Contracts.Spec.ApprovableLedgerInterface qualified as AL
import Lorentz.Contracts.Test.ApprovableLedger (AlSettings(..))
import Lorentz.Contracts.Test.SMT.Common
import Lorentz.Macro (mkView_)
import Lorentz.Value
import Morley.Michelson.Typed (SomeConstant)
import Test.Cleveland
import Test.Cleveland.Lorentz (contractConsumer)

----------------------------------------------------------------------------
-- Types
----------------------------------------------------------------------------

data Action
  = ATransfer ATransferParams
  | AApprove  AApproveParams
  deriving stock (Generic)
  deriving anyclass Buildable

data ATransferParams = ATransferParams
  { atpSender :: Addr
  , atpFrom   :: Addr
  , atpTo     :: Addr
  , atpAmount :: Natural
  }
  deriving stock Generic
  deriving anyclass Buildable

data AApproveParams = AApproveParams
  { aapSender  :: Addr
  , aapSpender :: Addr
  , aapAmount :: Natural
  }
  deriving stock (Generic)
  deriving anyclass Buildable

data EngineLedgerEntry = EngineLedgerEntry
  { _eleBalance   :: Natural
  , _eleApprovals :: HashMap Addr Natural
  } deriving stock (Eq, Generic)
    deriving anyclass Buildable

makeLenses ''EngineLedgerEntry

data EngineInProgressState = EngineInProgressState
  { _esLedger      :: HashMap Addr EngineLedgerEntry
  , _esTotalSupply :: Natural
  } deriving stock (Generic)
    deriving anyclass Buildable

makeLenses ''EngineInProgressState

data GetterAction
  = AGetTotalSupply
  | AGetBalance Addr
  | AGetAllowance Addr Addr

instance Buildable GetterAction where
  build = \case
    AGetTotalSupply ->
      "get total supply"
    AGetBalance owner ->
      "get balance of address " <> build owner
    AGetAllowance owner spender ->
      "get approval value of " <> build owner <> " for spender " <> build spender

instance Show GetterAction where
  show = pretty

type AlFixture = Fixture Action EngineInProgressState

type AlEngineState = EngineState EngineInProgressState

----------------------------------------------------------------------------
-- Utils
----------------------------------------------------------------------------

esBalanceOf :: Addr -> Lens' EngineInProgressState Natural
esBalanceOf owner = esLedger . at owner . non emptyELE . eleBalance

esApprovalOf :: Addr -> Addr -> Lens' EngineInProgressState Natural
esApprovalOf owner spender =
  esLedger . at owner . non emptyELE . eleApprovals . at spender . non 0

emptyELE :: EngineLedgerEntry
emptyELE = EngineLedgerEntry { _eleBalance = 0, _eleApprovals = mempty}

----------------------------------------------------------------------------
-- Generators
----------------------------------------------------------------------------

genAction :: MonadGen m => EngineInProgressState -> m (Action, AlEngineState)
genAction es = do
    action <- Gen.choice
      [ do
          atpSender <- anyAddr
          atpFrom <- mostlyWithAllowance atpSender
          atpTo <- anyAddrExcept atpFrom
          atpAmount <- mostlyAllowedTransfer atpSender atpFrom
          return $ ATransfer $ ATransferParams {..}
      , do
          aapSender <- anyAddr
          aapSpender <- mostlyWithoutAllowance aapSender
          aapAmount <- anyBalance
          return $ AApprove $ AApproveParams {..}
      ]
    return (action, applyAction action es)
  where
    mostlyWithoutAllowance owner = Gen.frequency
      [ withFreq 4 . elementOr owner $
          filter (\addr -> 0 == es ^. esApprovalOf owner addr) addrPool
      , withFreq 1 anyAddr
      ]
    mostlyWithAllowance sender = Gen.frequency
      [ withFreq 4 . elementOr sender $
          filter (\owner -> 0 /= es ^. esApprovalOf owner sender) addrPool
      , withFreq 1 anyAddr
      ]
    mostlyAllowedTransfer sender owner = Gen.frequency
      -- Note: here we avoid transfer of 0, because their behavior is undefined
      [ withFreq 3 . pure . max 1 $
          min (es ^. esBalanceOf owner) (es ^. esApprovalOf owner sender)
      , withFreq 2 $ Gen.enum 1 maxGenBalance
      ]
    -- Note: here we avoid transfer to self, because the logic is ambiguous
    anyAddrExcept addr = Gen.element $ filter (/= addr) addrPool

genGetterAction :: MonadGen m => EngineInProgressState -> m GetterAction
genGetterAction _es = Gen.choice
  [ pure AGetTotalSupply
  , AGetBalance <$> anyAddr
  , AGetAllowance <$> anyAddr <*> anyAddr
  ]

initSettingsToEngineState :: AlSettings -> AlEngineState
initSettingsToEngineState (AlInitAddresses initAddrs) =
    EsInProgress EngineInProgressState
    { _esLedger      = fromList $ map toLedgerEntry initAddrs
    , _esTotalSupply = sum $ map snd initAddrs
    }
  where
    toLedgerEntry (addr, _eleBalance) = (addrInPool addr, EngineLedgerEntry {..})
    _eleApprovals = mempty

genInitAlFixture :: MonadGen m => m (AlSettings, AlFixture)
genInitAlFixture = genInitFixture initSettingsToEngineState genAction

----------------------------------------------------------------------------
-- Actions application
----------------------------------------------------------------------------

applyAction :: Action -> EngineInProgressState -> AlEngineState
applyAction action es = case action of
  ATransfer (ATransferParams {..})
    | Just errs <- anyFires
        [ enoughBalanceCheck atpFrom atpAmount
        , enoughAllowanceCheck atpFrom atpSender atpAmount
        ] -> EsErrored errs
    | otherwise -> continueWith es $ do
        debit atpFrom atpAmount
        credit atpTo atpAmount
        when (atpSender /= atpFrom) $ reduceAllowance atpFrom atpSender atpAmount
  AApprove (AApproveParams {..})
    | aapAmount /= 0 && approvalOf aapSender aapSpender /= 0 ->
        EsErrored . (:| []) $ customError #unsafeAllowanceChange (approvalOf aapSender aapSpender)
    | otherwise -> continueWith es $ setAllowance aapSender aapSpender aapAmount
  where
    enoughBalanceCheck fromAddr amount =
      let fromAddrBal = es ^. esBalanceOf fromAddr in
      ( fromAddrBal < amount
      , customError #notEnoughBalance (#required :! amount, #present :! fromAddrBal)
      )
    enoughAllowanceCheck fromAddr senderAddr amount =
      let senderAllowance = approvalOf fromAddr senderAddr in
      ( senderAddr /= fromAddr && senderAllowance < amount
      , customError #notEnoughAllowance (#required :! amount, #present :! senderAllowance)
      )

    approvalOf owner spender = es ^. esApprovalOf owner spender

    debit addr amount = esBalanceOf addr -= amount
    credit addr amount = esBalanceOf addr += amount

    setAllowance owner spender amount = esApprovalOf owner spender .= amount
    reduceAllowance owner spender amount = esApprovalOf owner spender -= amount

applyGetterAction
  :: GetterAction
  -> EngineInProgressState
  -> Either (NonEmpty SomeConstant) Natural
applyGetterAction action es = return $ case action of
  AGetTotalSupply   -> es ^. esTotalSupply
  AGetBalance o     -> es ^. esBalanceOf o
  AGetAllowance o s -> es ^. esApprovalOf o s

actionsToScenario
  :: forall param st m caps. (MonadEmulated caps m, AL.ParameterC param)
  => (ContractHandle param st ())
  -> [Action]
  -> m ()
actionsToScenario alAddr actions = forM_ actions $ \action -> do
  actionToScenario alAddr action

actionToScenario
  :: forall param st m caps. (MonadEmulated caps m, AL.ParameterC param)
  => (ContractHandle param st ())
  -> Action
  -> m ()
actionToScenario alAddr = \case
  ATransfer (ATransferParams {..}) ->
    withAddrSender atpSender $ transfer alAddr $ calling (ep @"Transfer")
      (#from :! (toAddress $ addressInPool atpFrom), #to :! (toAddress $ addressInPool atpTo), #value :! atpAmount)
  AApprove (AApproveParams {..}) ->
    withAddrSender aapSender $ transfer alAddr $ calling (ep @"Approve")
      (#spender :! (toAddress $ addressInPool aapSpender), #value :! aapAmount)

getterActionToScenario
  :: (AL.ParameterC param, MonadEmulated caps m)
  => ContractHandle param st ()
  -> Either (NonEmpty SomeConstant) Natural
  -> GetterAction
  -> m ()
getterActionToScenario alAddr expected action =
  offshoot "One of getter validators" $ case action of
    AGetTotalSupply -> do
      consumer <- originate "consumer" def contractConsumer
      validateViewResult expected consumer $
        transfer alAddr $ calling (ep @"GetTotalSupply") (mkView_ () consumer)
    AGetBalance owner -> do
      let address = #owner :! (toAddress $ addressInPool owner)
      consumer <- originate "consumer" def contractConsumer
      validateViewResult expected consumer $
        transfer alAddr $ calling (ep @"GetBalance") (mkView_ address consumer)
    AGetAllowance owner spender -> do
      let ownerAddress = #owner :! (toAddress $ addressInPool owner)
          spenderAddress = #spender :! (toAddress $ addressInPool spender)
      consumer <- originate "consumer" def contractConsumer
      validateViewResult expected consumer $
        transfer alAddr $ calling (ep @"GetAllowance")
          (mkView_ (ownerAddress, spenderAddress) consumer)

----------------------------------------------------------------------------
-- TestTree(s)
----------------------------------------------------------------------------

-- | State Machine tests for a contract implementing the 'ApprovableLedger'
-- interface
approvableLedgerSMT
  :: forall param st. AL.ParameterC param
  => (forall caps m. MonadEmulated caps m
     => ImplicitAddressWithAlias -> AlSettings -> m (ContractHandle param st ())
     )
  -> [TestTree]
approvableLedgerSMT alOriginate =
  -- We want to make sure that there is no big distortion in number of
  -- ok/failure scenarios.
  -- Time wasted on generating later discarded scenarios should be negligible.
  [ testProperty "terminates normally" $
    withDiscards 300 $ withTests 50 $ property $ do
      (initSettings, Fixture{..}) <- forAll genInitAlFixture
      case fFinalState of
        EsErrored{} -> discard
        EsInProgress fes -> do
          gActions <- forAll $ Gen.list (Range.linear 0 100) (genGetterAction fes)
          testScenarioProps $ scenarioEmulated $ do
            alAddr <- alOriginate adminAddress initSettings
            validateFinalState fFinalState $ actionsToScenario alAddr fActions
            forM_ gActions $ \gAction -> do
              let expected = applyGetterAction gAction fes
              getterActionToScenario alAddr expected gAction

  , testProperty "ends with error" $
    withDiscards 300 $ withTests 50 $ property $ do
      (initSettings, Fixture{..}) <- forAll genInitAlFixture
      guard (esIsErrored fFinalState)
      testScenarioProps $ scenarioEmulated $ do
        alAddr <- alOriginate adminAddress initSettings
        validateFinalState fFinalState $ actionsToScenario alAddr fActions
  ]
