-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | State Machine Tests implementation for contracts satisfying the interface
-- of 'ManagedLedger'

module Lorentz.Contracts.Test.SMT.ManagedLedger
  ( managedLedgerSMT
  ) where

import Control.Lens (at, makeLenses, non, (+=), (-=), (.=))
import Fmt (Buildable(..), pretty)
import Hedgehog (MonadGen, discard, forAll, property, withDiscards, withTests)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import Text.Show qualified

import Lorentz.Contracts.Spec.ManagedLedgerInterface qualified as ML
import Lorentz.Contracts.Test.ApprovableLedger (AlSettings(..))
import Lorentz.Contracts.Test.SMT.ApprovableLedger (AApproveParams(..), ATransferParams(..))
import Lorentz.Contracts.Test.SMT.Common
import Lorentz.Macro (mkView_)
import Lorentz.Value
import Morley.Michelson.Typed (SomeConstant)
import Test.Cleveland
import Test.Cleveland.Lorentz (contractConsumer)

----------------------------------------------------------------------------
-- Types
----------------------------------------------------------------------------

data Action
  = ATransfer         ATransferParams
  | AApprove          AApproveParams
  | AApproveCAS       AApproveCASParams
  | ASetPause         ASetPauseParams
  | ASetAdministrator ASetAdministratorParams
  | AMint             AMintParams
  | ABurn             ABurnParams
  deriving stock (Generic)
  deriving anyclass Buildable

data AApproveCASParams = AApproveCASParams
  { aacpSender   :: Addr
  , aacpSpender  :: Addr
  , aacpAmount   :: Natural
  , aacpExpected :: Natural
  }
  deriving stock (Generic)
  deriving anyclass Buildable

data ASetPauseParams = ASetPauseParams
  { asppSender :: Addr
  , asppPause  :: Bool
  }
  deriving stock (Generic)
  deriving anyclass Buildable

data ASetAdministratorParams = ASetAdministratorParams
  { asapSender   :: Addr
  , asapNewAdmin :: Addr
  }
  deriving stock (Generic)
  deriving anyclass Buildable

data AMintParams = AMintParams
  { ampSender :: Addr
  , ampTo     :: Addr
  , ampAmount :: Natural
  }
  deriving stock (Generic)
  deriving anyclass Buildable

data ABurnParams = ABurnParams
  { abpSender :: Addr
  , abpFrom   :: Addr
  , abpAmount :: Natural
  }
  deriving stock (Generic)
  deriving anyclass Buildable

data EngineLedgerEntry = EngineLedgerEntry
  { _eleBalance   :: Natural
  } deriving stock (Eq, Generic)
    deriving anyclass Buildable

makeLenses ''EngineLedgerEntry

data ApprovalsKey = ApprovalsKey
  { _akOwner   :: Addr
  , _akSpender :: Addr
  } deriving stock (Eq, Generic)
    deriving anyclass Hashable
    deriving anyclass Buildable

data EngineInProgressState = EngineInProgressState
  { _esLedger        :: HashMap Addr EngineLedgerEntry
  , _esApprovals     :: HashMap ApprovalsKey Natural
  , _esTotalSupply   :: Natural
  , _esAdministrator :: Addr
  , _esPaused        :: Bool
  } deriving stock (Generic)
    deriving anyclass Buildable

makeLenses ''EngineInProgressState

data GetterAction res where
  AGetTotalSupply   :: GetterAction Natural
  AGetBalance       :: Addr -> GetterAction Natural
  AGetAllowance     :: Addr -> Addr -> GetterAction Natural
  AGetAdministrator :: GetterAction Addr

instance Buildable (GetterAction res) where
  build = \case
    AGetTotalSupply ->
      "get total supply"
    AGetBalance owner ->
      "get balance of address " <> build owner
    AGetAllowance owner spender ->
      "get approval value of " <> build owner <> " for spender " <> build spender
    AGetAdministrator ->
      "get token administrator"

data SomeGetterAction = forall res. SomeGetterAction (GetterAction res)

instance Show SomeGetterAction where
  show (SomeGetterAction a) = pretty a

type MlFixture = Fixture Action EngineInProgressState

type MlEngineState = EngineState EngineInProgressState

----------------------------------------------------------------------------
-- Utils
----------------------------------------------------------------------------

esBalanceOf :: Addr -> Lens' EngineInProgressState Natural
esBalanceOf owner = esLedgerEntry owner . eleBalance

esApprovalOf :: Addr -> Addr -> Lens' EngineInProgressState Natural
esApprovalOf _akOwner _akSpender = esApprovals . at ApprovalsKey {..} . non 0

esLedgerEntry :: Addr -> Lens' EngineInProgressState EngineLedgerEntry
esLedgerEntry owner = esLedger . at owner . non emptyELE

emptyELE :: EngineLedgerEntry
emptyELE = EngineLedgerEntry { _eleBalance = 0 }

----------------------------------------------------------------------------
-- Generators
----------------------------------------------------------------------------

genAction :: MonadGen m => EngineInProgressState -> m (Action, MlEngineState)
genAction es = do
    action <- Gen.frequency
      [ withFreq (rareIfPaused 8) $ do
          atpSender <- anyAddr
          atpFrom <- mostlyWithAllowance atpSender
          atpTo <- anyAddr
          atpAmount <- mostlyAllowedTransfer atpSender atpFrom
          return $ ATransfer $ ATransferParams {..}
      , withFreq (rareIfPaused 10) $ do
          aapSender <- anyAddr
          aapSpender <- mostlyWithoutAllowance aapSender
          aapAmount <- anyBalance
          return $ AApprove $ AApproveParams {..}
      , withFreq (rareIfPaused 3) $ do
          aacpSender <- anyAddr
          aacpSpender <- anyAddr
          aacpAmount <- anyBalance
          aacpExpected <- anyBalance
          return $ AApproveCAS $ AApproveCASParams {..}
      , withFreq 1 $ do
          asppSender <- mostlyAdmin
          asppPause <- Gen.bool
          return $ ASetPause $ ASetPauseParams {..}
      , withFreq 2 $ do
          asapSender <- mostlyAdmin
          asapNewAdmin <- anyAddr
          return $ ASetAdministrator $ ASetAdministratorParams {..}
      , withFreq 5 $ do
          ampSender <- mostlyAdmin
          ampTo <- anyAddr
          ampAmount <- anyBalance
          return $ AMint $ AMintParams {..}
      , withFreq 5 $ do
          abpSender <- mostlyAdmin
          abpFrom <- mostlyWithBalance
          abpAmount <- mostlyUnderBalanceOf abpFrom
          return $ ABurn $ ABurnParams {..}
      ]
    return (action, applyAction action es)
  where
    rareIfPaused n = if es ^. esPaused then 1 else n

    mostlyWithoutAllowance owner = Gen.frequency
      [ withFreq 4 . elementOr owner $
          filter (\addr -> 0 == es ^. esApprovalOf owner addr) addrPool
      , withFreq 1 anyAddr
      ]
    mostlyWithAllowance sender = Gen.frequency
      [ withFreq 4 . elementOr sender $
          filter (\owner -> 0 /= es ^. esApprovalOf owner sender) addrPool
      , withFreq 1 anyAddr
      ]
    mostlyAllowedTransfer sender owner = Gen.frequency
      [ withFreq 3 . pure $
          min (es ^. esBalanceOf owner) (es ^. esApprovalOf owner sender)
      , withFreq 2 anyBalance
      ]
    mostlyAdmin = Gen.frequency [(9, pure adminAddr), (1, anyAddr)]
    mostlyWithBalance = Gen.frequency
      [ withFreq 4 . elementOr adminAddr $
          filter (\addr -> 0 /= es ^. esBalanceOf addr) addrPool
      , withFreq 1 anyAddr
      ]
    mostlyUnderBalanceOf addr = Gen.frequency
      [ withFreq 4 $ Gen.integral (Range.linear 0 (es ^. esBalanceOf addr))
      , withFreq 1 $ anyBalance
      ]


genGetterAction :: MonadGen m => EngineInProgressState -> m SomeGetterAction
genGetterAction _es = Gen.choice
  [ pure $ SomeGetterAction AGetTotalSupply
  , SomeGetterAction ... AGetBalance <$> anyAddr
  , SomeGetterAction ... AGetAllowance <$> anyAddr <*> anyAddr
  , pure $ SomeGetterAction AGetAdministrator
  ]

initSettingsToEngineState :: AlSettings -> MlEngineState
initSettingsToEngineState (AlInitAddresses initAddrs) =
    EsInProgress EngineInProgressState
    { _esLedger        = fromList $ map toLedgerEntry initAddrs
    , _esApprovals     = mempty
    , _esTotalSupply   = sum $ map snd initAddrs
    , _esAdministrator = adminAddr
    , _esPaused        = False
    }
  where
    toLedgerEntry (addr, _eleBalance) = (addrInPool addr, EngineLedgerEntry {..})

genInitMlFixture :: MonadGen m => m (AlSettings, MlFixture)
genInitMlFixture = genInitFixture initSettingsToEngineState genAction

----------------------------------------------------------------------------
-- Actions application
----------------------------------------------------------------------------

applyAction :: Action -> EngineInProgressState -> MlEngineState
applyAction action es = case action of
  ATransfer (ATransferParams {..})
    | Just errs <- anyFires
        [ notPausedCheck
        , enoughBalanceCheck atpFrom atpAmount
        , enoughAllowanceCheck atpFrom atpSender atpAmount
        ] -> EsErrored errs
    | otherwise -> continueWith es $ do
        debit atpFrom atpAmount
        credit atpTo atpAmount
        when (atpSender /= atpFrom) $ reduceAllowance atpFrom atpSender atpAmount
  AApprove (AApproveParams {..})
    | Just errs <- anyFires
        [ notPausedCheck
        , safeAllowanceCheck aapSender aapSpender aapAmount
        ] -> EsErrored errs
    | otherwise -> continueWith es $ setAllowance aapSender aapSpender aapAmount
  AApproveCAS (AApproveCASParams {..})
    | Just errs <- anyFires
        [ notPausedCheck
        , allowanceMatchCheck aacpSender aacpSpender aacpExpected
        ] -> EsErrored errs
    | otherwise -> continueWith es $ setAllowance aacpSender aacpSpender aacpAmount
  ASetPause (ASetPauseParams {..})
    | notAdmin asppSender -> EsErrored (notAdminError :| [])
    | otherwise -> continueWith es $ esPaused .= asppPause
  ASetAdministrator (ASetAdministratorParams {..})
    | notAdmin asapSender -> EsErrored (notAdminError :| [])
    | otherwise -> continueWith es $ esAdministrator .= asapNewAdmin
  AMint (AMintParams {..})
    | notAdmin ampSender -> EsErrored (notAdminError :| [])
    | otherwise -> continueWith es $ do
        credit ampTo ampAmount
        esTotalSupply += ampAmount
  ABurn (ABurnParams {..})
    | Just errs <- anyFires
        [ ( notAdmin abpSender, notAdminError)
        , enoughBalanceCheck abpFrom abpAmount
        ] -> EsErrored errs
    | otherwise -> continueWith es $ do
        debit abpFrom abpAmount
        esTotalSupply -= abpAmount
  where
    notPausedCheck = (es ^. esPaused, customError #tokenOperationsArePaused ())
    enoughBalanceCheck fromAddr amount =
      let fromAddrBal = es ^. esBalanceOf fromAddr in
      ( fromAddrBal < amount
      , customError #notEnoughBalance (#required :! amount, #present :! fromAddrBal)
      )
    enoughAllowanceCheck fromAddr senderAddr amount =
      let senderAllowance = approvalOf fromAddr senderAddr in
      ( senderAddr /= fromAddr && senderAllowance < amount
      , customError #notEnoughAllowance (#required :! amount, #present :! senderAllowance)
      )
    safeAllowanceCheck senderAddr spenderAddr amount =
      ( amount /= 0 && approvalOf senderAddr spenderAddr /= 0
      , customError #unsafeAllowanceChange (approvalOf senderAddr spenderAddr)
      )
    allowanceMatchCheck senderAddr spenderAddr expectedAllowance =
      let currentAllowance = approvalOf senderAddr spenderAddr in
      ( currentAllowance /= expectedAllowance
      , customError #allowanceMismatch (#actual :! currentAllowance, #expected :! expectedAllowance)
      )

    approvalOf owner spender = es ^. esApprovalOf owner spender

    debit addr amount = esBalanceOf addr -= amount
    credit addr amount = esBalanceOf addr += amount

    setAllowance owner spender amount = esApprovalOf owner spender .= amount
    reduceAllowance owner spender amount = esApprovalOf owner spender -= amount

    notAdmin senderAddr = es ^. esAdministrator /= senderAddr
    notAdminError = customError #senderIsNotAdmin ()

applyGetterAction
  :: GetterAction res
  -> EngineInProgressState
  -> Either (NonEmpty SomeConstant) res
applyGetterAction action es = return $ case action of
  AGetTotalSupply   -> es ^. esTotalSupply
  AGetBalance o     -> es ^. esBalanceOf o
  AGetAllowance o s -> es ^. esApprovalOf o s
  AGetAdministrator -> es ^. esAdministrator

actionsToScenario
  :: (ML.ParameterC param, MonadEmulated caps m)
  => (ContractHandle param st ())
  -> [Action]
  -> m ()
actionsToScenario mlAddr actions = forM_ actions $ \action -> do
  actionToScenario mlAddr action

actionToScenario
  :: forall param st caps m. (ML.ParameterC param, MonadEmulated caps m)
  => (ContractHandle param st ())
  -> Action
  -> m ()
actionToScenario mlAddr = \case
  ATransfer (ATransferParams {..}) ->
    withAddrSender atpSender $ transfer mlAddr $ calling (ep @"Transfer")
      (#from :! (toAddress $ addressInPool atpFrom), #to :! (toAddress $ addressInPool atpTo), #value :! atpAmount)
  AApprove (AApproveParams {..}) ->
    withAddrSender aapSender $ transfer mlAddr $ calling (ep @"Approve")
      (#spender :! (toAddress $ addressInPool aapSpender), #value :! aapAmount)
  AApproveCAS (AApproveCASParams {..}) ->
    withAddrSender aacpSender $ transfer mlAddr $ calling (ep @"ApproveCAS")
      (#spender :! (toAddress $ addressInPool aacpSpender), #value :! aacpAmount, #expected :! aacpExpected)
  ASetPause (ASetPauseParams {..}) ->
    withAddrSender asppSender $ transfer mlAddr $ calling (ep @"SetPause") asppPause
  ASetAdministrator (ASetAdministratorParams {..}) ->
    withAddrSender asapSender $ transfer mlAddr $ calling (ep @"SetAdministrator")
      (toAddress $ addressInPool asapNewAdmin)
  AMint (AMintParams {..}) ->
    withAddrSender ampSender $ transfer mlAddr $ calling (ep @"Mint")
      (#to :! (toAddress $ addressInPool ampTo), #value :! ampAmount)
  ABurn (ABurnParams {..}) ->
    withAddrSender abpSender $ transfer mlAddr $ calling (ep @"Burn")
      (#from :! (toAddress $ addressInPool abpFrom), #value :! abpAmount)

getterActionToScenario
  :: (ML.ParameterC param, MonadEmulated caps m)
  => (ContractHandle param st ())
  -> Either (NonEmpty SomeConstant) res
  -> GetterAction res
  -> m ()
getterActionToScenario mlAddr expected action =
  offshoot "One of getter validators" $ case action of
    AGetTotalSupply -> do
      consumer <- originate "consumer" def contractConsumer
      validateViewResult expected consumer $
        transfer mlAddr $ calling (ep @"GetTotalSupply") (mkView_ () consumer)
    AGetBalance owner -> do
      let address = #owner :! (toAddress $ addressInPool owner)
      consumer <- originate "consumer" def contractConsumer
      validateViewResult expected consumer $
        transfer mlAddr $ calling (ep @"GetBalance") (mkView_ address consumer)
    AGetAllowance owner spender -> do
      let ownerAddress = #owner :! (toAddress $ addressInPool owner)
          spenderAddress = #spender :! (toAddress $ addressInPool spender)
      consumer <- originate "consumer" def contractConsumer
      validateViewResult expected consumer $
        transfer mlAddr $ calling (ep @"GetAllowance")
          (mkView_ (ownerAddress, spenderAddress) consumer)
    AGetAdministrator -> do
      consumer <- originate "consumer" def contractConsumer
      validateViewResult (toAddress . addressInPool <$> expected) consumer $
        transfer mlAddr $ calling (ep @"GetAdministrator") (mkView_ () consumer)

----------------------------------------------------------------------------
-- TestTree(s)
----------------------------------------------------------------------------

-- | State Machine tests for a contract implementing the 'ApprovableLedger'
-- interface
managedLedgerSMT
  :: forall param st. ML.ParameterC param
  => (forall caps m. MonadEmulated caps m
    => ImplicitAddressWithAlias -> AlSettings -> m (ContractHandle param st ())
    )
  -> [TestTree]
managedLedgerSMT mlOriginate =
  -- We want to make sure that there is no big distortion in number of
  -- ok/failure scenarios.
  -- Time wasted on generating later discarded scenarios should be negligible.
  [ testProperty "terminates normally" $
    withDiscards 300 $ withTests 50 $ property $ do
      (initSettings, Fixture{..}) <- forAll genInitMlFixture
      case fFinalState of
        EsErrored{} -> discard
        EsInProgress fes -> do
          gActions <- forAll $ Gen.list (Range.linear 0 100) (genGetterAction fes)
          testScenarioProps $ scenarioEmulated $ do
            mlAddr <- mlOriginate adminAddress initSettings
            validateFinalState fFinalState $ actionsToScenario mlAddr fActions
            forM_ gActions $ \(SomeGetterAction gAction) -> do
              let expected = applyGetterAction gAction fes
              getterActionToScenario mlAddr expected gAction

  , testProperty "ends with error" $
    withDiscards 300 $ withTests 50 $ property $ do
      (initSettings, Fixture{..}) <- forAll genInitMlFixture
      guard (esIsErrored fFinalState)
      testScenarioProps $ scenarioEmulated $ do
        mlAddr <- mlOriginate adminAddress initSettings
        validateFinalState fFinalState $ actionsToScenario mlAddr fActions
  ]
