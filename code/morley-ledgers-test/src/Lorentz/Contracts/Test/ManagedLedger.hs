-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for managed ledger implementations (all of them).

module Lorentz.Contracts.Test.ManagedLedger
  ( OriginationParams(..)
  , OriginationFunction
  , mkOriginateFn
  , managedLedgerGenericTest
  , originateManagedLedger
  ) where

import Data.Map qualified as Map
import Test.Tasty

import Lorentz (Contract, mkView_, printLorentzContract)
import Lorentz.Contracts.ManagedLedger qualified as ML
import Lorentz.Contracts.Spec.ApprovableLedgerInterface qualified as AL
import Lorentz.Contracts.Test.ApprovableLedger
import Lorentz.Value
import Morley.Michelson.Parser (MichelsonSource(..))
import Morley.Michelson.Runtime hiding (transfer)
import Morley.Michelson.TypeCheck (TypeCheckOptions(..), typeCheckContract, typeCheckingWith)
import Test.Cleveland
import Test.Cleveland.Lorentz (contractConsumer, testContractCoversEntrypointsT)

data OriginationParams = OriginationParams
  { opBalances :: Map Address Natural
  , opAdmin :: Address
  }

type OriginationFunction
  = forall caps m. (MonadCleveland caps m)
  => ImplicitAddressWithAlias
  -> AlSettings
  -> m (ContractHandle ML.Parameter ML.Storage ())

mkOriginateFn
  :: forall caps m. MonadCleveland caps m
  => Contract ML.Parameter ML.Storage ()
  -> OriginationParams
  -> m (ContractHandle ML.Parameter ML.Storage ())
mkOriginateFn con op = do
  let name = "Managed Ledger"
      balance = [tz|1000u|]
      storage = ML.mkStorage (opAdmin op) (opBalances op)
      contract = con in originate name storage contract balance

-- | Originate one of the managed ledger contracts with a storage
-- returned created by given function which takes admin address and
-- initial balances.
-- If initial balance is 0, ledger is empty.
originateManagedLedger
  :: forall m param st. (OriginationParams -> m (ContractHandle param st ()))
  -> ImplicitAddressWithAlias
  -> AlSettings
  -> m (ContractHandle param st ())
originateManagedLedger orgFn admin_ (AlInitAddresses addresses) =
  orgFn $ OriginationParams
    { opBalances = Map.fromList $ map (first toAddress) $ filter ((/= 0) . snd) addresses
    , opAdmin = toAddress admin_
    }

initSettings :: ImplicitAddressWithAlias -> ImplicitAddressWithAlias -> Natural -> AlSettings
initSettings wallet1 wallet2 initBalance
  -- If the initial balance is 0 we don't bother creating addresses
  | initBalance == 0 = AlInitAddresses mempty
  | otherwise = AlInitAddresses $
      [ (wallet1, initBalance)
      , (wallet2, initBalance) ]

-- | A set of tests for all contracts which implement managed ledger.
managedLedgerGenericTest :: OriginationFunction -> TestTree
managedLedgerGenericTest mlOriginate = testGroup "A set of tests for all contracts which implement managed ledger"
  [ testGroup "Transfers authorized by admin"
      [ testScenario "Can mint tokens" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 0
          consumer <- originate "consumer" def contractConsumer

          withSender admin . transfer ml $ calling def $
            ML.Mint
            ( #to :! (toAddress wallet1)
            , #value :! 10
            )

          transfer ml $ calling def $ ML.GetBalance (mkView_ (#owner :! (toAddress wallet1)) consumer)

          getStorage consumer @@== [10]

      , testScenario "Can not transfer foreign money without approval" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 0

          expectCustomError #notEnoughAllowance (#required :! 8, #present :! 0) $
            withSender admin $ do
              transfer ml $ calling def $
                ML.Mint
                ( #to :! (toAddress wallet1)
                , #value :! 10
                )
              transfer ml $ calling def $
                ML.Transfer
                ( #from :! (toAddress wallet1)
                , #to :! (toAddress wallet2)
                , #value :! 8
                )

      , testScenario "Transferring 0 tokens does not create entry in the ledger" $ scenario $ do
          TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 10

          withSender wallet1 $ do
            transfer ml $ calling (ep @"Transfer") $
              ( #from :! (toAddress wallet1)
              , #to :! (toAddress wallet3)
              , #value :! 0
              )

          finalStorage <- getStorage @ML.Storage ml

          (getBigMapValueMaybe (ML.ledgerRPC finalStorage) (toAddress wallet3)) @@== Nothing

      , testScenario "Can burn money" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 0
          consumer <- originate "consumer" def contractConsumer

          withSender admin $
            transfer ml $ calling def $
              ML.Mint
              ( #to :! (toAddress wallet1)
              , #value :! 10
              )

          withSender wallet1 $
            transfer ml $ calling def $
              ML.Transfer
              ( #from :! (toAddress wallet1)
              , #to :! (toAddress wallet2)
              , #value :! 10
              )

          withSender admin $
            transfer ml $ calling def $
              ML.Burn
              ( #from :! (toAddress wallet2)
              , #value :! 9
              )

          transfer ml $ calling def $ ML.GetBalance (mkView_ (#owner :! (toAddress wallet1)) consumer)
          transfer ml $ calling def $ ML.GetBalance (mkView_ (#owner :! (toAddress wallet2)) consumer)

          getStorage consumer @@== [1, 0]
      ]
  , testGroup "setPause"
      [ testScenario "Pause prohibits transfers" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 10

          withSender admin $ do
            transfer ml $ calling def $ ML.SetPause True
          expectCustomError #tokenOperationsArePaused () $
            withSender wallet1 $ do
              transfer ml $ calling def $
                ML.Transfer
                ( #from :! (toAddress wallet1)
                , #to :! (toAddress wallet2)
                , #value :! 3
                )

      , testScenario "Pause prohibits approvals" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 10

          withSender admin $ do
            transfer ml $ calling def $ ML.SetPause True
          expectCustomError #tokenOperationsArePaused () $
            withSender wallet1 $ do
              transfer ml $ calling def $
                ML.Approve
                ( #spender :! (toAddress wallet2)
                , #value :! 3
                )

      , testScenario "Admin cannot do transfers even during pause" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 10

          withSender admin $ do
            transfer ml $ calling def $ ML.SetPause True
          expectCustomError #tokenOperationsArePaused () $
            withSender admin $ do
              transfer ml $ calling def $
                ML.Transfer
                ( #from :! (toAddress wallet1)
                , #to :! (toAddress wallet2)
                , #value :! 3
                )

      , testScenario "Only admin can pause the contract" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 10
          expectCustomError #senderIsNotAdmin () $
            withSender wallet1 $
              transfer ml $ calling def $ ML.SetPause True
      ]
  , testGroup "setAdministrator"
      [ testScenario "Admin can delegate his rights" $ scenario $ do
          TestParams {admin, admin2, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 10

          withSender admin $ do
            transfer ml $ calling def $ ML.SetAdministrator (toAddress admin2)
          withSender admin2 $ do
            transfer ml $ calling def $ ML.SetPause True

      , testScenario "Only admin can set a new admin" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 10

          expectCustomError #senderIsNotAdmin () $ withSender wallet1 $ do
            transfer ml $ calling def $ ML.SetAdministrator (toAddress wallet2)
      ]
  , testGroup "Total supply"
      [ testScenario "Is correct after multiple transfers" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 0
          consumer <- originate "consumer" def contractConsumer

          withSender admin $ do
            transfer ml $ calling def $
              ML.Mint
              ( #to :! (toAddress wallet1)
              , #value :! 20
              )
            transfer ml $ calling def $
              ML.Mint
              ( #to :! (toAddress wallet2)
              , #value :! 5
              )
            transfer ml $ calling def $
              ML.Burn
              ( #from :! (toAddress wallet2)
              , #value :! 1
              )

          withSender wallet1 $
            transfer ml $ calling def $
              ML.Transfer
              ( #from :! (toAddress wallet1)
              , #to :! (toAddress wallet2)
              , #value :! 10
              )

          transfer ml $ calling def $ ML.GetTotalSupply (mkView_ () consumer)

          getStorage consumer @@== [24]
      ]
  , testGroup "Fails when expected allowance is incorrect"
      [ testScenario "initial approve" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 0

          expectCustomError #allowanceMismatch (#actual :! 0, #expected :! 1) $
            transfer ml $ calling def $
              ML.ApproveCAS (#spender :! (toAddress wallet1), #value :! 5, #expected :! 1)

      , testScenario "multiple approvals (non-zero to non-zero)" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 0

          transfer ml $ calling def $
            ML.ApproveCAS (#spender :! (toAddress wallet1), #value :! 5, #expected 0)

          transfer ml $ calling def $
            ML.ApproveCAS (#spender :! (toAddress wallet1), #value :! 6, #expected 5)

          transfer ml $ calling def $
            ML.ApproveCAS (#spender :! (toAddress wallet1), #value :! 7, #expected 6)

          expectCustomError #allowanceMismatch (#actual :! 7, #expected :! 6) $
            transfer ml $ calling def $
              ML.ApproveCAS (#spender :! (toAddress wallet1), #value :! 8, #expected 6)

      , testScenario "allowance is updated" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 0
          consumer <- originate "consumer" def contractConsumer

          let
            getAllowance = transfer ml $ calling def $ ML.GetAllowance $
              mkView_ (#owner :! (toAddress wallet2), #spender (toAddress wallet1)) consumer

          withSender wallet2 $ transfer ml $ calling def $
            ML.ApproveCAS (#spender :! (toAddress wallet1), #value :! 5, #expected 0)
          getAllowance

          withSender wallet2 $ transfer ml $ calling def $
            ML.ApproveCAS (#spender :! (toAddress wallet1), #value :! 6, #expected 5)
          getAllowance

          getStorage consumer @@== [6, 5]

      , testScenario "approving 0 tokens does not create entry in the approvals map" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 1

          withSender wallet1 $
            transfer ml $ calling def $
              ML.ApproveCAS (#spender :! (toAddress wallet2), #value :! 0, #expected 0)

          finalStorage <- getStorage @ML.Storage ml

          (getBigMapValueMaybe (ML.approvalsRPC finalStorage) (#owner :! (toAddress wallet1), #spender :! (toAddress wallet2)) ) @@== Nothing
      ]
  , testGroup "Behaves according to the specification"
      [ testScenario "Can transfer zero from non-existent account, or account with zero money" $ scenario $ do
          TestParams {admin, wallet1, wallet2} <- genTestParams
          ml <- mlOriginate admin $ initSettings wallet1 wallet2 0
          transfer ml $ calling def $
            ML.Transfer
              ( #from :! (toAddress wallet1)
              , #to :! (toAddress wallet2)
              , #value :! 0
              )
      ]
  , testGroup "Has consistent ledger interface"
      [ testContractCoversEntrypointsT @AL.Parameter "Satisfies FA1.2 specification"
          ML.managedLedgerContract
      ]
  , testGroup "Typecheck ManagedLedger after printing"
      [ testScenario "Typechecks successfully" $ scenario $ do
          let
            mlText = printLorentzContract True ML.managedLedgerContract
          case (parseExpandContract MSUnspecified $ toStrict mlText) of
            Right c ->
              let options = TypeCheckOptions { tcVerbose = False, tcStrict = False }
              in case typeCheckingWith options $ typeCheckContract c of
                   Right _ -> pure ()
                   Left err -> runIO $ throwM err
            Left _ -> error "Parsing of printed ManagedLedger contract failed"
      ]
  ]
