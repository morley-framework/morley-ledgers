-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for managed ledger implementations (all of them).

module Lorentz.Contracts.Test.ApprovableLedger
  ( AlSettings(..)
  , AlOriginationFunction
  , genTestParams
  , TestParams(..)
  , approvableLedgerGenericTest
  , alEmptyInitBalanceTest
  , alTransferTest
  , alTransferSelfTest
  , alTransferSelfAlwaysTest
  , alAllowTest
  ) where

import Lorentz (mkView_)
import Lorentz.Contracts.Spec.ApprovableLedgerInterface qualified as AL
import Test.Cleveland.Lorentz (contractConsumer)
import Morley.Util.SizedList (SizedList'(..))
import Test.Cleveland
import Test.Tasty (TestTree, testGroup)

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

data TestParams = TestParams
  { admin   :: ImplicitAddressWithAlias
  , admin2  :: ImplicitAddressWithAlias
  , wallet1 :: ImplicitAddressWithAlias
  , wallet2 :: ImplicitAddressWithAlias
  , wallet3 :: ImplicitAddressWithAlias
  }

data AlSettings
  = AlInitAddresses [(ImplicitAddressWithAlias, Natural)]
  deriving stock (Show)

type AlOriginationFunction param st
  = forall caps m. MonadCleveland caps m
  => ImplicitAddressWithAlias
  -> AlSettings
  -> m (ContractHandle param st ())

-- | Common settings generator used in 'Spec's
initSettings
  :: ImplicitAddressWithAlias
  -> ImplicitAddressWithAlias
  -> ImplicitAddressWithAlias
  -> Natural
  -> AlSettings
initSettings wallet1 wallet2 wallet3 initBalance
  -- If the initial balance is 0 we don't bother creating addresses
  -- meaning that they do not appear in corresponding tests.
  | initBalance == 0 = AlInitAddresses mempty
  | otherwise = AlInitAddresses $
      [ (wallet1, initBalance)
      , (wallet2, initBalance)
      -- We pass this wallet with initial balance since it's used
      -- in some tests here, but also note that it may be confused with
      -- `managedLedgerGenericTest` where we don't bother initializing this wallet
      -- since we don't use it there.
      , (wallet3, 0)
      ]

-- | A set of tests for all contracts that implement the approvable ledger interface.
approvableLedgerGenericTest
  :: forall param st. AL.ParameterC param => AlOriginationFunction param st
  -> TestTree
approvableLedgerGenericTest alOriginate =
  testGroup "A set of tests for all contracts that implement the approvable ledger interface"
    [ alEmptyInitBalanceTest @param @st alOriginate
    , alTransferTest @param @st alOriginate
    , alTransferSelfTest @param @st alOriginate
    , alAllowTest @param @st alOriginate
    ]

alEmptyInitBalanceTest
  :: forall param st. AL.ParameterC param => AlOriginationFunction param st
  -> TestTree
alEmptyInitBalanceTest alOriginate =
  testScenario "Balance is initially empty" $ scenario $ do
    TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
    al <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 0
    consumer <- originate "consumer" def contractConsumer

    transfer al $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)
    getStorage consumer @@== [0]

-- | A set of tests for the @transfer@ entrypoint of contracts that implement
-- the approvable ledger interface.
-- This tests focus on @transfer@ alone, testing only cases where @approve@ does
-- not make a difference and/or is not necessary.
alTransferTest
  :: forall param st. AL.ParameterC param => AlOriginationFunction param st
  -> TestTree
alTransferTest alOriginate =
  testGroup "Transfers without allowance" $
    [ testScenario "Can transfer own money" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet2)
          , #value :! 5
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)
        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet2)) consumer)

        getStorage consumer @@== [15, 5]

    , testScenario "Cannot transfer foreign money" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10

        expectCustomError #notEnoughAllowance (#required :! 5, #present :! 0) $
          withSender wallet2 $ transfer ml $ calling (ep @"Transfer") $
            ( #from :! (toAddress wallet1)
            , #to :! (toAddress wallet2)
            , #value :! 5
            )

    , testScenario "Cannot transfer too much money" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10

        expectCustomError #notEnoughBalance (#required :! 11, #present :! 10) $
          withSender wallet1 $ transfer ml $ calling (ep @"Transfer") $
            ( #from :! (toAddress wallet1)
            , #to :! (toAddress wallet2)
            , #value :! 11
            )

    , testScenario "Transferring 0 tokens does not change balances" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet3)
          , #value :! 0
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)
        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet3)) consumer)

        getStorage consumer @@== [0, 10]
   ]
-- | Set of tests for the @transfer@ entrypoint that check that self-transfer
-- (where @from@ is equal to @to@) are checked as any other transfer.
-- Note that this is the correct behavior for a contract implementing the
-- abstract ledger interface, as opposed to the one described in 'alTransferSelfAlwaysSpec'.
alTransferSelfTest
  :: forall param st. AL.ParameterC param => AlOriginationFunction param st
  -> TestTree
alTransferSelfTest alOriginate =
  testGroup "Transfers to self"
    [ testScenario "Can transfer own money to self" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet1)
          , #value :! 5
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)

        getStorage consumer @@== [10]

    , testScenario "Cannnot make self-transfer bigger than owned money" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 5

        expectCustomError #notEnoughBalance (#required :! 10, #present :! 5) $
          withSender wallet1 $ transfer ml $ calling (ep @"Transfer") $
            ( #from :! (toAddress wallet1)
            , #to :! (toAddress wallet1)
            , #value :! 10
            )

    , testScenario "Sender 'A' cannot transfer from 'B' to 'B' without enough allowance" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10

        withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
          ( #spender :! (toAddress wallet2)
          , #value :! 5
          )

        expectCustomError #notEnoughAllowance (#required :! 10, #present :! 5) $
          withSender wallet2 $ transfer ml $ calling (ep @"Transfer") $
            ( #from :! (toAddress wallet1)
            , #to :! (toAddress wallet1)
            , #value :! 10
            )

    , testScenario "Sender 'A' can transfer from 'B' to 'B' with enough allowance" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
          ( #spender :! (toAddress wallet2)
          , #value :! 5
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)

        withSender wallet2 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet1)
          , #value :! 5
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)
        transfer ml $ calling (ep @"GetAllowance") $
          (mkView_ (#owner :! (toAddress wallet1), #spender :! (toAddress wallet2)) consumer)

        getStorage consumer @@== [0, 10, 10]
   ]
-- | A set of tests for the @transfer@ entrypoint of contracts that always allow
-- self-transfer, regardless of the balance of the account or allowance.
-- Note that this is not the correct behavior (see 'alTransferSelfSpec'), but some
-- contracts implementing the approvable ledger interface behave like this.
-- As such this test is not part of the general 'approvableLedgerGenericTest' and has
-- a warning associated to it.
{-# WARNING alTransferSelfAlwaysTest "testing special self-transfer behavior" #-}
alTransferSelfAlwaysTest
  :: forall param st. AL.ParameterC param => AlOriginationFunction param st
  -> TestTree
alTransferSelfAlwaysTest alOriginate =
  testGroup "Transfers to self"
    [ testScenario "Can transfer own money to self" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet1)
          , #value :! 5
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)
        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)

        getStorage consumer @@== [10, 10]

    , testScenario "Can make self-transfer bigger than owned money" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 5
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet1)
          , #value :! 10
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)
        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)

        getStorage consumer @@== [5, 5]
    ]
-- | A set of tests for the @approve@ and @transfer@ entrypoints of contracts
-- that implement the approvable ledger interface.
alAllowTest
  :: forall param st. AL.ParameterC param => AlOriginationFunction param st
  -> TestTree
alAllowTest alOriginate =
  testGroup "Allowance"
    [ testScenario "Can get allowance" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams

        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
          ( #spender :! (toAddress wallet2)
          , #value :! 5
          )

        transfer ml $ calling (ep @"GetAllowance") $
          (mkView_ (#owner :! (toAddress wallet1), #spender :! (toAddress wallet2)) consumer)

        withSender wallet2 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet2)
          , #value :! 5
          )

        transfer ml $ calling (ep @"GetAllowance") $
          (mkView_ (#owner :! (toAddress wallet1), #spender :! (toAddress wallet2)) consumer)

        getStorage consumer @@== [0, 5]

    , testScenario "Can spend foreign money with allowance" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
          ( #spender :! (toAddress wallet2)
          , #value :! 5
          )

        withSender wallet2 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet2)
          , #value :! 5
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet2)) consumer)

        getStorage consumer @@== [15]

    , testScenario "Cannot spend more than allowed amount of money" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10

        withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
          ( #spender :! (toAddress wallet2)
          , #value :! 5
          )

        expectCustomError #notEnoughAllowance (#required :! 3, #present :! 2) $ replicateM_ 2 $ do
          withSender wallet2 $ transfer ml $ calling (ep @"Transfer") $
            ( #from :! (toAddress wallet1)
            , #to :! (toAddress wallet2)
            , #value :! 3
            )

    , testScenario "Can close allowance suggestion" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10

        withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
          ( #spender :! (toAddress wallet2)
          , #value :! 5
          )
        withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
          ( #spender :! (toAddress wallet2)
          , #value :! 0
          )
        expectCustomError #notEnoughAllowance (#required :! 1, #present :! 0) $ do
          withSender wallet2 $ transfer ml $ calling (ep @"Transfer") $
            ( #from :! (toAddress wallet1)
            , #to :! (toAddress wallet2)
            , #value :! 1
            )

    , testScenario "Can transfer approved money to a foreign address" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10
        consumer <- originate "consumer" def contractConsumer

        withSender wallet1 $ transfer ml $ calling (ep @"Approve")$
          ( #spender :! (toAddress wallet2)
          , #value :! 5
          )
        withSender wallet2 $ transfer ml $ calling (ep @"Transfer") $
          ( #from :! (toAddress wallet1)
          , #to :! (toAddress wallet3)
          , #value :! 1
          )

        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet1)) consumer)
        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet2)) consumer)
        transfer ml $ calling (ep @"GetBalance") (mkView_ (#owner :! (toAddress wallet3)) consumer)

        getStorage consumer @@== [1, 10, 9]

    , testScenario "Cannot set allowance from non-zero to non-zero" $ scenario $ do
        TestParams {admin, wallet1, wallet2, wallet3} <- genTestParams
        ml <- alOriginate admin $ initSettings wallet1 wallet2 wallet3 10

        expectCustomError #unsafeAllowanceChange 5 $ do
          withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
            ( #spender :! (toAddress wallet2)
            , #value :! 5
            )
          withSender wallet1 $ transfer ml $ calling (ep @"Approve") $
            ( #spender :! (toAddress wallet2)
            , #value :! 3
            )
    ]

genTestParams
  :: forall caps m. (HasCallStack, MonadFail m, MonadCleveland caps m)
  => m TestParams
genTestParams = do
  admin ::< admin2 ::< wallet1 ::<
    wallet2 ::< wallet3 ::< Nil' <- newAddresses $ "admin" :< "admin2" :<
      "wallet1" :< "wallet2" :< "wallet3" :< Nil

  return TestParams {..}
