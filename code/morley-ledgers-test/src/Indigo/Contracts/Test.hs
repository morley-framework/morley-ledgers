-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Contracts.Test
  ( module Exports
  ) where

import Indigo.Contracts.Test.FA2 as Exports
