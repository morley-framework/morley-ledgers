-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Network tests for contracts implementing FA2 interface
-- https://gitlab.com/tzip/tzip/-/blob/1f83a3671cdff3ab4517bfa9ee5a57fc5276d4ff/proposals/tzip-12/tzip-12.md

module Indigo.Contracts.Test.FA2
  ( fa2NettestScenario
  ) where

import Data.Map.Strict qualified as Map
import Test.Tasty (TestTree, testGroup)

import Indigo.Contracts.FA2Sample as FA2 hiding (addOperator, transfer)
import Lorentz qualified as L (mkView_)
import Lorentz.Contracts.Spec.FA2Interface as FA2
import Lorentz.Value
import Morley.Util.Named
import Test.Cleveland
import Test.Cleveland.Lorentz (contractConsumer)
import Morley.Util.SizedList (SizedList'(..))

-- This function returns a Maybe type to allow the consumers of this test
-- to indicate wether the provided origination parameters is compatible
-- with the contract under test. If they are not, the calling code should
-- return a nothing, and the corresponding tests are skipped.
type OriginationFn param st =
  forall caps m. MonadCleveland caps m
  => OriginationParams
  -> m (Maybe (ContractHandle param st ()))

data OriginationParams = OriginationParams
  { opBalances :: LedgerInner
  , opOwnerToOperators :: Map Address [Address]
  , opOwner :: Address
  , opPermissionsDescriptor :: PermissionsDescriptor
  , opAllowedTokenIds :: [TokenId]
  , opTokenMetadata :: FA2.TokenMetadata
  }

generateDefaultOriginationParams :: Address -> OriginationParams
generateDefaultOriginationParams testOwner = OriginationParams
  { opBalances = mempty
  , opOwner = testOwner
  , opOwnerToOperators = mempty
  , opPermissionsDescriptor = defTestPermissionDescriptor
  , opAllowedTokenIds = [theTokenId]
  , opTokenMetadata = defaultTokenMetadata
  }

defaultTokenMetadata :: FA2.TokenMetadata
defaultTokenMetadata = Map.fromList
  [ ([mt|symbol|], "TestTokenSymbol")
  , ([mt|name|], "TestTokenName")
  , ([mt|decimals|], "8")
  ]

configFromOrigParams :: OriginationParams -> FA2Config
configFromOrigParams OriginationParams{..} = FA2Config
  { cPermissionsDescriptor = opPermissionsDescriptor
  , cAllowedTokenIds = opAllowedTokenIds
  }
addAccountWithToken :: (Address, TokenId, ([Address], Natural)) -> OriginationParams -> OriginationParams
addAccountWithToken (addr, tokenId, (operators, bal)) op = let
  withAccount = op
    { opBalances =
        -- Insert for token id 0 for the time being (because our sample contracts and test only
        -- considers a single token type.
        Map.insert (addr, tokenId) bal $ opBalances op
    }
  in Prelude.foldl' (\oparams operator -> addOperator (addr, operator) oparams) withAccount operators

addAccount :: (Address, ([Address], Natural)) -> OriginationParams -> OriginationParams
addAccount (addr, (operators, bal)) op =
  addAccountWithToken (addr, theTokenId, (operators, bal)) op

addOperator :: (Address, Address) -> OriginationParams -> OriginationParams
addOperator (owner_, operator) op = op
  { opOwnerToOperators =
      Map.alter (\case
          Just ops -> Just $ operator:ops
          Nothing -> Just [operator]) owner_ $ opOwnerToOperators op
  }

mkView
  :: forall paramFa a r contract. ToContractRef r contract
  => a
  -> contract
  -> FA2View paramFa a r
mkView a c = FA2View (L.mkView_ a c)

opToStorage :: OriginationParams -> Storage
opToStorage OriginationParams {..} = let
  operators = Map.foldrWithKey foldFn mempty opOwnerToOperators
  in mkStorage defaultTokenMetadata opBalances (mkBigMap operators)
  where
    foldFn
      :: Address
      -> [Address]
      -> Map (Address, Address) ()
      -> Map (Address, Address) ()
    foldFn ow ops m = foldr (\a b -> Map.insert (ow, a) () b) m ops

permissionDescriptorNoTransfer :: PermissionsDescriptor
permissionDescriptorNoTransfer =
  defTestPermissionDescriptor { pdOperator = NoTransfer }

permissionDescriptorOwnerTransfer :: PermissionsDescriptor
permissionDescriptorOwnerTransfer =
  defTestPermissionDescriptor { pdOperator = OwnerTransfer }

tokenId1, tokenId2 :: TokenId
tokenId1 = TokenId 0
tokenId2 = TokenId 1

originationFn :: OriginationFn FA2.FA2SampleParameter FA2.Storage
originationFn op = do
  -- We return a `Just` value here because we have parameterized the contract
  -- using the permission discriptor variants.
  cHandler <- originate "FA2Test"
    (opToStorage op)
    (fa2Contract (configFromOrigParams op))

  return $ Just $ cHandler

fa2NettestScenario :: [TestTree]
fa2NettestScenario = fa2scenario @FA2.FA2SampleParameter @FA2.Storage originationFn

-- General FA2 tests and infra
withOriginated
  :: forall param st caps m. MonadCleveland caps m
  => OriginationFn param st
  -> OriginationParams
  -> (ContractHandle param st () -> m ())
  -> m ()
withOriginated originateFn op tests = originateFn op >>= \case
  Just contract -> tests contract
  Nothing -> pure () -- The contract does not support the parameters so we skip the tests.

fa2scenario
  :: forall param st. ParameterC param
  => OriginationFn param st
  -> [TestTree]
fa2scenario originateFn =
  [ generalFA2Tests @param @st originateFn
  , selfTransferTests @param @st originateFn
  ]

generalFA2Tests
  :: forall param st. ParameterC param
  => OriginationFn param st
  -> TestTree
generalFA2Tests originateFn =
  testGroup "FA2 scenarios"
    [ testScenario "Allows zero transfer from non-existent accounts" $ scenario $ do
        TestParams { defaultOriginationParams, walletDst, nonexistent } <- testParams
        let
          originationParams = addAccount (toAddress walletDst, ([], 10)) $ defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers = [TransferItem (toAddress nonexistent) [TransferDestination (toAddress walletDst) tokenId1 0]]
          withSender nonexistent $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "Is denied if only owner transfer is allowed" $ scenario $ do
        TestParams { defaultOriginationParams, operator, walletSrc, walletDst } <- testParams
        let
          originationParams = addAccount (toAddress walletSrc, ([toAddress operator], 10)) $
            defaultOriginationParams { opPermissionsDescriptor = permissionDescriptorOwnerTransfer }
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers = [TransferItem (toAddress walletSrc) [TransferDestination (toAddress walletDst) tokenId1 1]]
          expectCustomError #fA2_NOT_OWNER () $
            withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "aborts if there is a failure (due to low balance)" $ scenario $ do
        TestParams { defaultOriginationParams, operator, walletSrc, walletDst } <- testParams
        let
          originationParams = addAccount (toAddress walletSrc, ([toAddress operator], 10)) defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers =
                [ TransferItem (toAddress walletSrc)
                  [ TransferDestination (toAddress walletDst) tokenId1 10
                    , TransferDestination (toAddress walletDst) tokenId1 10
                  ]
                ]
          expectCustomError #fA2_INSUFFICIENT_BALANCE (#required :! 10, #present :! 0) $
            withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers


    , testScenario "aborts if there is a failure (due to non existent source account)" $ scenario $ do
        TestParams { defaultOriginationParams, operator, walletSrc, walletDst } <- testParams
        let originationParams = addOperator (toAddress walletSrc, toAddress operator) $
              addAccount (toAddress walletDst, ([], 10)) defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers = [TransferItem (toAddress walletSrc) [TransferDestination (toAddress walletDst) tokenId1 1]]
          expectCustomError #fA2_INSUFFICIENT_BALANCE (#required :! 1, #present :! 0) $
            withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers


    , testScenario "aborts if there is a failure (due to bad operator)" $ scenario $ do
        TestParams { defaultOriginationParams, operator, wallet1, wallet2, wallet3, wallet4, wallet5 } <- testParams
        let originationParams = addAccount (toAddress wallet1, ([toAddress operator], 10))
              $ addAccount (toAddress wallet2, ([toAddress operator], 0))
              $ addAccount (toAddress wallet3, ([], 0))
              $ addAccount (toAddress wallet4, ([toAddress operator], 0)) defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers1 = [TransferItem (toAddress wallet1) [TransferDestination (toAddress wallet2) tokenId1 1]]
          let transfers2 =
                [ TransferItem (toAddress wallet3)
                  [ TransferDestination (toAddress wallet1) tokenId1 1
                  , TransferDestination (toAddress wallet4) tokenId1 1
                  , TransferDestination (toAddress wallet5) tokenId1 1
                  ]
                ]

          let transfers = transfers1 <> transfers2
          expectCustomError #fA2_NOT_OPERATOR () $
            withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "aborts if there is a failure (due to bad operator for zero transfer)" $ scenario $ do
        TestParams { defaultOriginationParams, operator, wallet1, wallet2, wallet3, wallet4 } <- testParams
        let originationParams = addAccount (toAddress wallet1, ([toAddress operator], 10))
              $ addAccount (toAddress wallet2, ([toAddress operator], 0))
              $ addAccount (toAddress wallet3, ([], 0))
              $ addAccount (toAddress wallet4, ([toAddress operator], 0)) defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let
            transfers1 = [TransferItem (toAddress wallet1) [TransferDestination (toAddress wallet2) tokenId1 1]]
            transfers2 = [TransferItem (toAddress wallet3) [TransferDestination (toAddress wallet2) tokenId1 0]]

            transfers = transfers1 <> transfers2
          expectCustomError #fA2_NOT_OPERATOR () $
            withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "accepts an empty list of transfers" $ scenario $ do
        TestParams { defaultOriginationParams, operator, wallet1 } <- testParams
        let originationParams =
              addAccount (toAddress wallet1, ([toAddress operator], 10)) $ defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let
            transfers = [TransferItem { tiFrom = toAddress wallet1, tiTxs = [] }]
          withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "is denied if operator transfer is denied in permissions descriptior" $ scenario $ do
        TestParams { defaultOriginationParams, operator, wallet1, wallet2 } <- testParams
        let originationParams = addAccount (toAddress wallet1, ([toAddress operator], 10)) $
              defaultOriginationParams
                { opPermissionsDescriptor = permissionDescriptorOwnerTransfer }
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers = [TransferItem (toAddress wallet1) [TransferDestination (toAddress wallet2) tokenId1 1]]
          expectCustomError #fA2_NOT_OWNER () $
            withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "validates token id" $ scenario $ do
        TestParams { defaultOriginationParams, operator, wallet1, wallet2, wallet3, wallet4 } <- testParams
        let originationParams =
              addAccount (toAddress wallet1, ([toAddress operator], 10)) $ defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let
            transfers =
              [ TransferItem
                { tiFrom = toAddress wallet1
                , tiTxs =
                    [ TransferDestination { tdTo = toAddress wallet2, tdTokenId = tokenId1, tdAmount = 5 }
                    , TransferDestination { tdTo = toAddress wallet3, tdTokenId = tokenId2, tdAmount = 1 }
                    , TransferDestination { tdTo = toAddress wallet4, tdTokenId = tokenId1, tdAmount = 4 }
                    ]
                }
              ]
          expectCustomError #fA2_TOKEN_UNDEFINED () $
            withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "cannot transfer foreign money" $ scenario $ do
        TestParams { defaultOriginationParams, operator, wallet1, wallet2 } <- testParams
        let originationParams = addAccount (toAddress wallet2, ([], 10)) $
              addAccount (toAddress wallet1, ([toAddress operator], 10)) $ defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers = [TransferItem (toAddress wallet2) [TransferDestination (toAddress wallet1) tokenId1 1]]
          expectCustomError #fA2_NOT_OPERATOR () $
            withSender operator $ transfer fa2 $ calling (ep @"Transfer") transfers
    ]

selfTransferTests
  :: forall param st. ParameterC param
  => OriginationFn param st
  -> TestTree
selfTransferTests originateFn =
  testGroup "Self Transfer"
    [ testScenario "Cannot transfer foreign money" $ scenario $ do
        TestParams { defaultOriginationParams, wallet1, wallet2 } <- testParams
        let originationParams = (addAccount (toAddress wallet1, ([], 10)) defaultOriginationParams)
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers = [TransferItem (toAddress wallet1) [TransferDestination (toAddress wallet2) tokenId1 1]]
          expectCustomError #fA2_NOT_OPERATOR () $
            withSender wallet2 $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "is denied if self transfer is forbidden in permissions descriptior" $ scenario $ do
        TestParams { defaultOriginationParams, wallet1, wallet2 } <- testParams
        let originationParams = addAccount (toAddress wallet1, ([], 10)) $
              defaultOriginationParams { opPermissionsDescriptor = permissionDescriptorNoTransfer }
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers = [TransferItem (toAddress wallet1) [TransferDestination (toAddress wallet2) tokenId1 1]]
          expectCustomError #fA2_TX_DENIED () $
            withSender wallet1 $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "validates token id" $ scenario $ do
        TestParams { defaultOriginationParams, operator, wallet1, wallet2, wallet3, wallet4 } <- testParams
        let originationParams =
              addAccount (toAddress wallet1, ([toAddress operator], 10)) $ defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let
            transfers =
              [ TransferItem
                  { tiFrom = toAddress wallet1
                  , tiTxs =
                      [ TransferDestination { tdTo = toAddress wallet2, tdTokenId = tokenId1, tdAmount = 5 }
                      , TransferDestination { tdTo = toAddress wallet3, tdTokenId = tokenId2, tdAmount = 1 }
                      , TransferDestination { tdTo = toAddress wallet4, tdTokenId = tokenId1, tdAmount = 4 }
                      ]
                  }
              ]
          expectCustomError #fA2_TOKEN_UNDEFINED () $
            withSender wallet1 $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "aborts if there is a failure (due to low balance)" $ scenario $ do
        TestParams { defaultOriginationParams, operator, walletSrc, walletDst } <- testParams
        let
          originationParams = addAccount (toAddress walletSrc, ([toAddress operator], 10)) defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers =
                [ TransferItem (toAddress walletSrc)
                    [ TransferDestination (toAddress walletDst) tokenId1 10
                    , TransferDestination (toAddress walletDst) tokenId1 10
                    ]
                ]
          expectCustomError #fA2_INSUFFICIENT_BALANCE (#required :! 10, #present :! 0) $
            (withSender walletSrc $ transfer fa2 $ calling (ep @"Transfer") transfers)

    , testScenario "aborts if there is a failure (due to non existent source account)" $ scenario $ do
        TestParams { defaultOriginationParams, walletSrc, walletDst } <- testParams
        let originationParams = addAccount (toAddress walletDst, ([], 10)) defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let transfers = [TransferItem (toAddress walletSrc) [TransferDestination (toAddress walletDst) tokenId1 1]]
          expectCustomError #fA2_INSUFFICIENT_BALANCE (#required :! 1, #present :! 0) $
            withSender walletSrc $ transfer fa2 $ calling (ep @"Transfer") transfers


    , testScenario "aborts if there is a failure (due to bad token id)" $ scenario $ do
        TestParams { defaultOriginationParams, wallet1, wallet2, wallet3, wallet4, wallet5 } <- testParams
        let originationParams = addAccount (toAddress wallet1, ([], 10)) defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let
            transfers =
              [ TransferItem
                { tiFrom = toAddress wallet1
                , tiTxs =
                    [ TransferDestination { tdTo = toAddress wallet2, tdTokenId = tokenId1, tdAmount = 1 }
                    , TransferDestination { tdTo = toAddress wallet3, tdTokenId = tokenId1, tdAmount = 1 }
                    , TransferDestination { tdTo = toAddress wallet4, tdTokenId = tokenId2, tdAmount = 1 } -- Should fail
                    , TransferDestination { tdTo = toAddress wallet5, tdTokenId = tokenId1, tdAmount = 1 }
                    ]
                }
              ]
          expectCustomError #fA2_TOKEN_UNDEFINED () $
            withSender wallet1 $ transfer fa2 $ calling (ep @"Transfer") transfers

    , testScenario "balanceOf request validates token id" $ scenario $ do
        TestParams { defaultOriginationParams, wallet1 } <- testParams
        let originationParams =
              addAccount (toAddress wallet1, ([], 10)) $ defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          consumer <- originate "consumer" [] contractConsumer
          let
            balanceRequestItem = BalanceRequestItem { briOwner = toAddress wallet1, briTokenId = tokenId2 }
            balanceRequest = mkView [balanceRequestItem] consumer

          expectCustomError #fA2_TOKEN_UNDEFINED () $
            withSender wallet1 $ transfer fa2 $ calling (ep @"Balance_of") balanceRequest

    , testScenario "balanceOf request accepts empty list" $ scenario $ do
        TestParams { defaultOriginationParams, wallet1 } <- testParams
        let originationParams =
              addAccount (toAddress wallet1, ([], 10)) $ defaultOriginationParams
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          consumer <- originate "consumer" [] contractConsumer
          let
            balanceRequestItems = []
            balanceRequest = mkView balanceRequestItems consumer

          withSender wallet1 $ transfer fa2 $ calling (ep @"Balance_of") balanceRequest

    , testScenario "Configure operators entrypoint throws error if transfers are disabled" $ scenario $ do
        TestParams { defaultOriginationParams, wallet1, wallet2 } <- testParams
        let originationParams = addAccount (toAddress wallet1, ([], 10)) $ defaultOriginationParams
              { opPermissionsDescriptor = permissionDescriptorNoTransfer }
        withOriginated @param @st originateFn originationParams $ \fa2 -> do
          let operatorParam = OperatorParam { opOwner = toAddress wallet1, opOperator = toAddress wallet2, opTokenId = tokenId1 }
          let addOperatorParam = AddOperator operatorParam
          expectCustomError #fA2_OPERATORS_UNSUPPORTED () $
            withSender wallet1 $ transfer fa2 $ calling (ep @"Update_operators") [addOperatorParam]
    ]

----------------------------------------------------------------------------------------------------
-- Helpers
----------------------------------------------------------------------------------------------------

data TestParams = TestParams
  { defaultOriginationParams :: OriginationParams
  , owner       :: ImplicitAddressWithAlias
  , operator    :: ImplicitAddressWithAlias
  , walletSrc   :: ImplicitAddressWithAlias
  , walletDst   :: ImplicitAddressWithAlias
  , nonexistent :: ImplicitAddressWithAlias
  , wallet1     :: ImplicitAddressWithAlias
  , wallet2     :: ImplicitAddressWithAlias
  , wallet3     :: ImplicitAddressWithAlias
  , wallet4     :: ImplicitAddressWithAlias
  , wallet5     :: ImplicitAddressWithAlias
  }


testParams
  :: forall caps m. (HasCallStack, MonadFail m, MonadCleveland caps m)
  => m TestParams
testParams = do
  -- Originate involved addresses
  owner ::< operator ::< walletSrc ::< walletDst ::< nonexistent ::<
    wallet1 ::< wallet2 ::< wallet3 ::< wallet4 ::< wallet5 ::< Nil'
    <- newAddresses
      $ "owner" :< "operator" :< "walletSrc" :< "walletDst" :< "nonexistent"
      :< "wallet1" :< "wallet2" :< "wallet3" :< "wallet4" :< "wallet5"
      :< Nil
  let
    defaultOriginationParams = generateDefaultOriginationParams (toAddress owner)

  return TestParams {..}
