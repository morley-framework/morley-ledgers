-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for managed ledger implementations (all of them).

module Test.Lorentz.Contracts.ManagedLedger
  ( test_ManagedLedgerLorentz
  , test_ManagedLedgerIndigo
  , test_ManagedLedgerSMT
  , test_ManagedLedgerIndigoSMT
  , test_Documentation
  ) where

-- Import Prelude to make intero calm
import Prelude

import Test.Tasty (TestTree, testGroup)

import Indigo.Contracts.ManagedLedger qualified as I
import Lorentz.Contracts.ManagedLedger qualified as ML
import Test.Cleveland.Lorentz

import Lorentz.Contracts.Test

originateLorentz :: OriginationFunction
originateLorentz admin_ settings =
  originateManagedLedger (mkOriginateFn ML.managedLedgerContract) admin_ settings

originateIndigo :: OriginationFunction
originateIndigo admin_ settings =
  originateManagedLedger (mkOriginateFn I.managedLedgerContract) admin_ settings

test_ManagedLedgerLorentz :: TestTree
test_ManagedLedgerLorentz =
  testGroup "Approvable and Managed Ledgers tests with Lorentz origination" $
    [ approvableLedgerGenericTest @ML.Parameter @ML.Storage  originateLorentz
    , managedLedgerGenericTest originateLorentz
    ]
test_ManagedLedgerIndigo :: TestTree
test_ManagedLedgerIndigo =
  testGroup "Approvable and Managed Ledgers tests with Indigo origination" $
    [ approvableLedgerGenericTest @ML.Parameter @ML.Storage originateIndigo
    , managedLedgerGenericTest originateIndigo
    ]

test_ManagedLedgerSMT :: [TestTree]
test_ManagedLedgerSMT =
  [ testGroup "Lorentz approvable ledger state machine tests" $
      approvableLedgerSMT @ML.Parameter @ML.Storage originateLorentz
  , testGroup "Lorentz managed ledger state machine tests" $
      managedLedgerSMT @ML.Parameter @ML.Storage originateLorentz
  ]

test_ManagedLedgerIndigoSMT :: [TestTree]
test_ManagedLedgerIndigoSMT =
  [ testGroup "Indigo approvable ledger state machine tests" $
      approvableLedgerSMT @ML.Parameter @ML.Storage originateIndigo
  , testGroup "Indigo managed ledger state machine tests" $
      managedLedgerSMT @ML.Parameter @ML.Storage originateIndigo
  ]

test_Documentation :: [TestTree]
test_Documentation =
  [ testGroup "Lorentz managed ledger documentation" $
      runDocTests testLorentzDoc ML.managedLedgerContract
  , testGroup "Indigo managed ledger documentation" $
      runDocTests testLorentzDoc I.managedLedgerContract
  ]
