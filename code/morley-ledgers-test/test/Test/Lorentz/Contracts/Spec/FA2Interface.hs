-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.Contracts.Spec.FA2Interface
  ( test_Annotations
  ) where

import Test.Tasty
import Test.Tasty.HUnit

import Lorentz (FollowEntrypointFlag(FollowEntrypoint), HasAnnotation(getAnnotation))
import Morley.Michelson.Typed (Notes(..))
import Morley.Michelson.Untyped.Annotation (annQ, noAnn)

import Indigo.Contracts.FA2Sample
import Lorentz.Contracts.Spec.FA2Interface

test_Annotations :: TestTree
test_Annotations =
  testGroup "FA2 annotations"
    [ testCase "TransferParams" $
        -- https://gitlab.com/tzip/tzip/-/blob/1f83a3671cdff3ab4517bfa9ee5a57fc5276d4ff/proposals/tzip-12/tzip-12.md#transfer
        getAnnotation @TransferParams FollowEntrypoint @?=
          NTList noAnn
            (NTPair noAnn [annQ|from_|] [annQ|txs|] noAnn noAnn
              (NTAddress noAnn)
              (NTList noAnn
                (NTPair noAnn [annQ|to_|] noAnn noAnn noAnn
                  (NTAddress noAnn)
                  (NTPair noAnn [annQ|token_id|] [annQ|amount|] noAnn noAnn
                    (NTNat noAnn)
                    (NTNat noAnn)
                  )
                )
              )
            )

    , testCase "BalanceRequestParams" $
        -- https://gitlab.com/tzip/tzip/-/blob/1f83a3671cdff3ab4517bfa9ee5a57fc5276d4ff/proposals/tzip-12/tzip-12.md#balance_of
        getAnnotation @BalanceRequestParams FollowEntrypoint @?=
          NTPair noAnn [annQ|requests|] [annQ|callback|] noAnn noAnn
            (NTList noAnn
              (NTPair noAnn [annQ|owner|] [annQ|token_id|] noAnn noAnn
                (NTAddress noAnn)
                (NTNat noAnn)
              )
            )
            (NTContract noAnn
              (NTList noAnn
                (NTPair noAnn [annQ|request|] [annQ|balance|] noAnn noAnn
                  (NTPair noAnn [annQ|owner|] [annQ|token_id|] noAnn noAnn
                    (NTAddress noAnn)
                    (NTNat noAnn)
                  )
                  (NTNat noAnn)
                )
              )
            )


    , testCase "UpdateOperatorsParam" $
        -- https://gitlab.com/tzip/tzip/-/blob/1f83a3671cdff3ab4517bfa9ee5a57fc5276d4ff/proposals/tzip-12/tzip-12.md#update_operators
        getAnnotation @UpdateOperatorsParam FollowEntrypoint @?=
          NTList noAnn
            (NTOr noAnn [annQ|add_operator|] [annQ|remove_operator|]
              (NTPair noAnn [annQ|owner|] noAnn noAnn noAnn
                (NTAddress noAnn)
                (NTPair noAnn [annQ|operator|] [annQ|token_id|] noAnn noAnn
                  (NTAddress noAnn)
                  (NTNat noAnn)
                )
              )
              (NTPair noAnn [annQ|owner|] noAnn noAnn noAnn
                (NTAddress noAnn)
                (NTPair noAnn [annQ|operator|] [annQ|token_id|] noAnn noAnn
                  (NTAddress noAnn)
                  (NTNat noAnn)
                )
              )
            )

    , testCase "PermissionsDescriptorParam" $
        -- https://gitlab.com/tzip/tzip/-/blob/ed7fbfe859a7c245d067d732c8cb2ce158d63a4f/proposals/tzip-12/tzip-12.md#exposing-permissions-descriptor
        getAnnotation @PermissionsDescriptorParam FollowEntrypoint @?=
          NTContract noAnn
            (NTPair noAnn [annQ|operator|] noAnn noAnn noAnn
              (NTOr noAnn [annQ|no_transfer|] noAnn
                (NTUnit noAnn)
                (NTOr noAnn [annQ|owner_transfer|] [annQ|owner_or_operator_transfer|]
                  (NTUnit noAnn)
                  (NTUnit noAnn)
                )
              )
              (NTPair noAnn [annQ|receiver|] noAnn noAnn noAnn
                (NTOr noAnn [annQ|owner_no_hook|] noAnn
                  (NTUnit noAnn)
                  (NTOr noAnn [annQ|optional_owner_hook|] [annQ|required_owner_hook|]
                    (NTUnit noAnn)
                    (NTUnit noAnn)
                  )
                )
                (NTPair noAnn [annQ|sender|] [annQ|custom|] noAnn noAnn
                  (NTOr noAnn [annQ|owner_no_hook|] noAnn
                    (NTUnit noAnn)
                    (NTOr noAnn [annQ|optional_owner_hook|] [annQ|required_owner_hook|]
                      (NTUnit noAnn)
                      (NTUnit noAnn)
                    )
                  )
                  (NTOption noAnn
                    (NTPair noAnn [annQ|tag|] [annQ|config_api|] noAnn noAnn
                      (NTString noAnn)
                      (NTOption noAnn
                        (NTAddress noAnn)
                      )
                    )
                  )
                )
              )
            )

    , testCase "TransferDescriptorParam" $
        -- https://gitlab.com/tzip/tzip/-/blob/ed7fbfe859a7c245d067d732c8cb2ce158d63a4f/proposals/tzip-12/tzip-12.md#token-owner-hook-permission-behavior
        getAnnotation @TransferDescriptorParam FollowEntrypoint @?=
          NTPair noAnn [annQ|batch|] [annQ|operator|] noAnn noAnn
            (NTList noAnn
              (NTPair noAnn [annQ|from_|] [annQ|txs|] noAnn noAnn
                (NTOption noAnn
                  (NTAddress noAnn)
                )
                (NTList noAnn
                  (NTPair noAnn [annQ|to_|] noAnn noAnn noAnn
                    (NTOption noAnn
                      (NTAddress noAnn)
                    )
                    (NTPair noAnn [annQ|token_id|] [annQ|amount|] noAnn noAnn
                      (NTNat noAnn)
                      (NTNat noAnn)
                    )
                  )
                )
              )
            )
            (NTAddress noAnn)

    , testCase "Sample Storage" $
        -- This test ensures the storage in the example contract
        -- has a big_map field annotated with "token_metadata", as per the FA2 spec.
        --
        -- > Contract storage MUST have a big_map that maps token_id -> token_metadata_michelson
        -- > and annotated %token_metadata
        getAnnotation @Storage FollowEntrypoint @?=
          NTPair noAnn [annQ|ledger|] noAnn noAnn noAnn
            (NTBigMap noAnn
              (NTPair noAnn noAnn noAnn noAnn noAnn
                (NTAddress noAnn)
                (NTNat noAnn)
              )
              (NTNat noAnn)
            )
            (NTPair noAnn [annQ|operators|] [annQ|token_metadata|] noAnn noAnn
              (NTBigMap noAnn
                (NTPair noAnn noAnn noAnn noAnn noAnn
                  (NTAddress noAnn)
                  (NTAddress noAnn)
                )
                (NTUnit noAnn)
              )
              (NTBigMap noAnn
                (NTNat noAnn)
                (NTMap noAnn (NTString noAnn) (NTBytes noAnn))
              )
            )
    ]
