-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Indigo.Contracts.FA2
  ( test_FA2Doc
  , test_Nettests
  ) where

import Indigo.Contracts.FA2Sample
import Lorentz.Value
import Test.Cleveland.Lorentz
import Test.Tasty (TestTree)

import Indigo.Contracts.Test

test_FA2Doc :: [TestTree]
test_FA2Doc = runDocTests testLorentzDoc (fa2Contract def)

test_Nettests :: [TestTree]
test_Nettests = fa2NettestScenario
