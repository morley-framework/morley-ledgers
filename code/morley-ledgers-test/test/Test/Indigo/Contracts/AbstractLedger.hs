-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- TODO morley/#653: update this module
{-# OPTIONS_GHC -Wno-orphans #-}

module Test.Indigo.Contracts.AbstractLedger
  ( test_AbstractLedger
  , test_Documentation
  ) where

import Fmt (Buildable(..))
import Test.Tasty (TestTree, testGroup)
import Debug qualified (show)

import Indigo.Contracts.AbstractLedger
import Lorentz (TAddress(..), toTAddress, mkView_, toMichelsonContract)
import Morley.Michelson.Runtime.GState (genesisAddress)
import Morley.Michelson.Typed qualified as T
import Morley.Tezos.Address (Address, detGenKeyAddress)
import Morley.Util.Named
import Test.Cleveland
import Test.Cleveland.Lorentz (contractConsumer)
import Test.Cleveland.Doc (runDocTests, testLorentzDoc)
import Test.Cleveland.Michelson.Import (testTreesWithTypedContract)

data ContractType = Morley | Lorentz

-- | All tests for AbstractLedger (FA1).
test_AbstractLedger :: IO [TestTree]
test_AbstractLedger = do
  morleyTree  <- testTreesWithTypedContract "test/resources/FA1.tz" (pure . testImpl Morley)
  let lorentzTree = testImpl Lorentz $ toMichelsonContract abstractLedgerContract

  return
    [ testGroup "Morley version" morleyTree
    , testGroup "Lorentz version" lorentzTree
    ]
  where
    testImpl :: ContractType -> TContract -> [TestTree]
    testImpl cty fa1 = ($ fa1) <$>
      [ testTransfer cty
      , testGetTotalSupply
      , testGetBalance
      ]

test_Documentation :: [TestTree]
test_Documentation =
  runDocTests testLorentzDoc abstractLedgerContract

type TStorage = T.ToT Storage
type TContract = T.Contract (T.ToT Parameter) TStorage
type Validator =
  forall caps m. MonadEmulated caps m
  => ContractHandle Parameter Storage () -- the tested contract handle
  -> ContractHandle Natural [Natural] () -- the consumer contract handle
  -> m () -- the test action itself
  -> m ()

instance Buildable Storage where
  build = Debug.show

source, foo, bar :: Address
source  = toAddress $ genesisAddress
foo     = toAddress $ detGenKeyAddress "foo"
bar     = toAddress $ detGenKeyAddress "bar"

storage :: Storage
storage = Storage
  { sLedger = T.mkBigMap
    [ (bar,    300)
    , (source, 200)
    ]
  , sTotalSupply = 500
  }

-- | A contract specification factory.
makeTestCase
  :: TContract
  -> String
  -> Validator
  -> (TAddress Natural () -> Parameter)
  -> TestTree
makeTestCase contract name validator param = testScenarioOnEmulator name $ scenarioEmulated do
  handle <- originate (fromString name) storage (TypedContract @_ @_ @() contract)
  -- NB: Right now, the consumer contract is originated unconditionally; while
  -- testing on the emulator only, this doesn't matter, but if we ever change
  -- these tests to run on network, we'll need to make this origination lazy.
  featherHandle <- originate "feather" [] contractConsumer
  validator handle featherHandle $ transfer handle $ calling def (param $ toTAddress featherHandle)

-- | Testing various properties of `GetBalance`.
testGetBalance
  :: TContract
  -> TestTree
testGetBalance contract = testGroup "GetBalance"
  [ makeTestCase'
      "just gets balance for recorded address"
      gettingExistingBalance
      $ GetBalance . mkView_ (#owner bar)

  , makeTestCase'
      "just gets balance for missing address"
      gettingNonExistingBalance
      $ GetBalance . mkView_ (#owner foo)
  ]
  where
    makeTestCase' = makeTestCase contract

-- | Testing properties of `GetBalance` when dest address is present.
gettingExistingBalance
  :: Validator
gettingExistingBalance hndl fh action = do
  action
  getFullStorage hndl @@== storage
  getStorage fh @@== [300]

-- | Testing properties of `GetBalance` when dest address is NOT present.
gettingNonExistingBalance
  :: Validator
gettingNonExistingBalance hndl fh action = do
  action
  getFullStorage hndl @@== storage
  getStorage fh @@== [0]

-- | Testing properties of `GetTotalSupply`.
testGetTotalSupply :: TContract -> TestTree
testGetTotalSupply contract = testGroup "GetTotalSupply"
  [ makeTestCase contract "getting" gettingTotalSupply
      $ GetTotalSupply . mkView_ ()
  ]

-- | Checking that `GetTotalSupply` doesn't change the storage
--   and returns total supply to the provided address.
gettingTotalSupply :: Validator
gettingTotalSupply hndl fh action = do
  action
  getFullStorage hndl @@== storage
  getStorage fh @@== [500]

-- | Testing properties of `Transfer`.
testTransfer :: ContractType -> TContract -> TestTree
testTransfer cty contract = testGroup "Transfer"
  [ makeTestCase' "paying"          success
    $ const $ Transfer (#from source, #to foo, #value 10)
  , makeTestCase' "paying 0"        payNothing
    $ const $ Transfer (#from source, #to foo, #value 0)
  , makeTestCase' "paying to self"  nothingChanges
    $ const $ Transfer (#from source, #to source, #value 10)
  , makeTestCase' "paying more than you have" (overdraft2 cty)
    $ const $ Transfer (#from source, #to foo, #value 500)
  , makeTestCase' "paying to self more than you have" (selfOverdraft cty)
    $ const $ Transfer (#from source, #to source, #value 500)
  , makeTestCase' "paying from someone to self" (overdraft cty)
    $ const $ Transfer (#from foo, #to source, #value 500)
  ]
  where
    makeTestCase' = makeTestCase contract

-- | Checking that paying more that you have fails.
overdraft :: ContractType -> Validator
overdraft cty _ _ action = action & case cty of
  Morley -> expectFailedWith ()
  Lorentz -> expectCustomError #nonAcceptableSource (#sender :! source, #from :! foo)

-- | Checking that paying more that you have fails.
overdraft2 :: ContractType -> Validator
overdraft2 cty _ _ action = action & case cty of
  Morley -> expectFailedWith ()
  Lorentz -> expectCustomError #notEnoughBalance (#required :! 500, #present :! 200)

-- | Checking that no changes to storage were made.
nothingChanges :: Validator
nothingChanges hndl fh action = do
  action
  getFullStorage hndl @@== storage
  getStorage fh @@== []

-- | Checking that you can't pay to youself more than you have.
selfOverdraft :: ContractType -> Validator
selfOverdraft cty _ _ action = action & case cty of
  Morley -> expectFailedWith ()
  Lorentz -> expectCustomError #notEnoughBalance (#required :! 500, #present :! 200)

-- | Checking that test payment was successful.
success
  :: Validator
success hndl _ action = do
  action
  getFullStorage hndl @@==
    Storage
      (T.mkBigMap
        [ (source, 190 :: Natural)
        , (foo,    10)
        , (bar,    300)
        ])
      500

-- | Checking that paying of 0 creates a record of 0.
payNothing :: Validator
payNothing hndl _ action = do
  action
  getFullStorage hndl @@==
    Storage
      (T.mkBigMap
        [ (source, 200 :: Natural)
        , (foo,    0)
        , (bar,    300)
        ])
      500
